SUMMARY = "generated recipe based on hunspell-tpi srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-tpi = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-tpi-0.07-10.el8.noarch.rpm \
          "

SRC_URI[hunspell-tpi.sha256sum] = "79687f12216e42314361553db9fd5ae27ea4f126ee306ae5bdcc577cf5a5790b"
