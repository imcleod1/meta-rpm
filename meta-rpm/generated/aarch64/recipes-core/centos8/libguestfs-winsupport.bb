SUMMARY = "generated recipe based on libguestfs-winsupport srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_libguestfs-winsupport = "libguestfs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libguestfs-winsupport-8.0-4.module_el8.2.0+320+13f867d7.aarch64.rpm \
          "

SRC_URI[libguestfs-winsupport.sha256sum] = "953d0f60f2c2758e83126e2d407c221ffe8eed40211b12375d8fc5735266a01b"
