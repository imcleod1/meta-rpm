SUMMARY = "generated recipe based on tar srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "acl libselinux pkgconfig-native"
RPM_SONAME_REQ_tar = "ld-linux-aarch64.so.1 libacl.so.1 libc.so.6 libselinux.so.1"
RDEPENDS_tar = "bash glibc info libacl libselinux"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/tar-1.30-4.el8.aarch64.rpm \
          "

SRC_URI[tar.sha256sum] = "51ee9e5981361b491a22f63223798ebfa2287af8bb6206b4f8538e945b37fcf2"
