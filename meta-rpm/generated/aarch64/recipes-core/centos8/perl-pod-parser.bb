SUMMARY = "generated recipe based on perl-Pod-Parser srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Pod-Parser = "perl-Carp perl-Exporter perl-Getopt-Long perl-PathTools perl-Pod-Usage perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-Pod-Parser-1.63-396.el8.noarch.rpm \
          "

SRC_URI[perl-Pod-Parser.sha256sum] = "d1a7407665e22118ca137036998b4df1a879c6d24e8b103d4dfa034bc5be2d03"
