SUMMARY = "generated recipe based on perl-inc-latest srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-inc-latest = "perl-Carp perl-ExtUtils-Install perl-File-Path perl-IO perl-PathTools perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-inc-latest-0.500-9.el8.noarch.rpm \
          "

SRC_URI[perl-inc-latest.sha256sum] = "f51794211ca2647d555c1fcca921b9b892d9c7844d6bb386d9f0c829892e4ba1"
