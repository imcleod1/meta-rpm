SUMMARY = "generated recipe based on mdadm srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_mdadm = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libpthread.so.0"
RDEPENDS_mdadm = "bash chkconfig coreutils glibc libreport-filesystem systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/mdadm-4.1-13.el8.aarch64.rpm \
          "

SRC_URI[mdadm.sha256sum] = "d507c49d0d8cba9c23fea660733ba9bc0f70cf4b69d8ba1b395b0eb983c737d4"
