SUMMARY = "generated recipe based on libotf srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "freetype libice libsm libx11 libxaw libxmu libxt pkgconfig-native"
RPM_SONAME_PROV_libotf = "libotf.so.0"
RPM_SONAME_REQ_libotf = "ld-linux-aarch64.so.1 libICE.so.6 libSM.so.6 libX11.so.6 libXaw.so.7 libXmu.so.6 libXt.so.6 libc.so.6 libfreetype.so.6"
RDEPENDS_libotf = "freetype glibc libICE libSM libX11 libXaw libXmu libXt"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/libotf-0.9.13-11.el8.aarch64.rpm \
          "

SRC_URI[libotf.sha256sum] = "7d9cb2531abea6b114ff6247a27b1b390db01d89d953e34f1ca22a63bf8df548"
