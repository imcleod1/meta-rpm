SUMMARY = "generated recipe based on python-lxml srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libxml2 libxslt pkgconfig-native platform-python3 zlib"
RPM_SONAME_REQ_python3-lxml = "ld-linux-aarch64.so.1 libc.so.6 libexslt.so.0 libm.so.6 libpthread.so.0 libpython3.6m.so.1.0 librt.so.1 libxml2.so.2 libxslt.so.1 libz.so.1"
RDEPENDS_python3-lxml = "glibc libxml2 libxslt platform-python python3-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-lxml-4.2.3-1.el8.aarch64.rpm \
          "

SRC_URI[python3-lxml.sha256sum] = "285433b7a928884e5602680b591057089c142c7c48129345febbd992168e12ef"
