SUMMARY = "generated recipe based on libkeepalive srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libkeepalive = "libkeepalive.so"
RPM_SONAME_REQ_libkeepalive = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2"
RDEPENDS_libkeepalive = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libkeepalive-0.3-8.el8.aarch64.rpm \
          "

SRC_URI[libkeepalive.sha256sum] = "b570ebea350b6bf9f3bdfa67b426f88f8c7b6f662b6c78e783db50f40e045229"
