SUMMARY = "generated recipe based on lua-posix srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libxcrypt ncurses pkgconfig-native"
RPM_SONAME_REQ_lua-posix = "ld-linux-aarch64.so.1 libc.so.6 libcrypt.so.1 libncursesw.so.6 librt.so.1 libtinfo.so.6"
RDEPENDS_lua-posix = "glibc libxcrypt lua-libs ncurses-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/lua-posix-33.3.1-9.el8.aarch64.rpm \
          "

SRC_URI[lua-posix.sha256sum] = "d5b190da69a359842b1a8950ba26b9f1409158174cb7b43954c338ec3750a2cd"
