SUMMARY = "generated recipe based on hyphen-hi srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-3.0"
RPM_LICENSE = "LGPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-hi = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hyphen-hi-0.7.0-11.el8.noarch.rpm \
          "

SRC_URI[hyphen-hi.sha256sum] = "4f9030d8fbb11e56eb54dace95385fbc9edfa00f4b9a01d6ce840768b29cc795"
