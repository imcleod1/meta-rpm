SUMMARY = "generated recipe based on jboss-logging-tools srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED & LGPL-2.0"
RPM_LICENSE = "ASL 2.0 and LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_jboss-logging-tools = "java-1.8.0-openjdk-headless javapackages-filesystem jdeparser"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/jboss-logging-tools-2.0.1-6.el8.noarch.rpm \
          "

SRC_URI[jboss-logging-tools.sha256sum] = "bce2396ac1676bfba9954036479b9f0db3ea69208522d40fbe047a12ddc7c0cb"
