SUMMARY = "generated recipe based on usbutils srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libusb1 pkgconfig-native systemd-libs"
RPM_SONAME_REQ_usbutils = "ld-linux-aarch64.so.1 libc.so.6 libudev.so.1 libusb-1.0.so.0"
RDEPENDS_usbutils = "bash glibc hwdata libusbx platform-python systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/usbutils-010-3.el8.aarch64.rpm \
          "

SRC_URI[usbutils.sha256sum] = "3527e7e12dbeebd972b7ff3cc420e06420d58a96289046b56a76ab6af6af28e5"
