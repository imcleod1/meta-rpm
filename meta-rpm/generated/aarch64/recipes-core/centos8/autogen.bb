SUMMARY = "generated recipe based on autogen srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-3.0"
RPM_LICENSE = "LGPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "guile libxml2 pkgconfig-native xz zlib"
RPM_SONAME_REQ_autogen = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2 libguile-2.0.so.22 liblzma.so.5 libm.so.6 libopts.so.25 libpthread.so.0 libxml2.so.2 libz.so.1"
RDEPENDS_autogen = "autogen-libopts bash glibc guile info libxml2 perl-Carp perl-Exporter perl-Scalar-List-Utils perl-Text-ParseWords perl-constant perl-interpreter perl-libs xz-libs zlib"
RPM_SONAME_PROV_autogen-libopts = "libopts.so.25"
RPM_SONAME_REQ_autogen-libopts = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_autogen-libopts = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/autogen-libopts-5.18.12-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/autogen-5.18.12-7.el8.aarch64.rpm \
          "

SRC_URI[autogen.sha256sum] = "480113d9cd264707bbbb4bdc1786ebe760f7a48814eea346f928447fb53a4ebf"
SRC_URI[autogen-libopts.sha256sum] = "f6d5d8fc16c2730f70a39ee16343fb500b7dcb27fde93a183d403a3b130f54a6"
