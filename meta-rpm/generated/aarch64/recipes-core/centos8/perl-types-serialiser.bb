SUMMARY = "generated recipe based on perl-Types-Serialiser srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Types-Serialiser = "perl-Carp perl-common-sense perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Types-Serialiser-1.0-12.el8.noarch.rpm \
          "

SRC_URI[perl-Types-Serialiser.sha256sum] = "95066b775b87ec2acef0a35d073856502a7a95f58956440396c5072e8b3def45"
