SUMMARY = "generated recipe based on glibc srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & LGPL-2.0 & GPL-2.0 & GPL-2.0 & BSD & CLOSED & ISC & CLOSED & GFDL-1.1"
RPM_LICENSE = "LGPLv2+ and LGPLv2+ with exceptions and GPLv2+ and GPLv2+ with exceptions and BSD and Inner-Net and ISC and Public Domain and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPROVIDES_glibc-langpack-ga = "locale-base-ga-ie (= 2.28) locale-base-ga-ie.utf8 (= 2.28) locale-base-ga-ie@euro (= 2.28) virtual-locale-ga (= 2.28) virtual-locale-ga (= 2.28) virtual-locale-ga (= 2.28) virtual-locale-ga-ie (= 2.28) virtual-locale-ga-ie.utf8 (= 2.28) virtual-locale-ga-ie@euro (= 2.28)"
RDEPENDS_glibc-langpack-ga = "glibc glibc-common"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-ga-2.28-101.el8.aarch64.rpm \
          "

SRC_URI[glibc-langpack-ga.sha256sum] = "c2d9ed39ba98a89bf6f32709ad01ab0a2373788343215cae532ec24910253986"
