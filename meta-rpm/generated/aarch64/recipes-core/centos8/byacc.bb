SUMMARY = "generated recipe based on byacc srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "Public Domain"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_byacc = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_byacc = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/byacc-1.9.20170709-4.el8.aarch64.rpm \
          "

SRC_URI[byacc.sha256sum] = "a2675092c2e80a9455b294014e4ad6c59ae7a549b8a9d9bda544d24ab2be765d"
