SUMMARY = "generated recipe based on librelp srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "gnutls-libs pkgconfig-native"
RPM_SONAME_PROV_librelp = "librelp.so.0"
RPM_SONAME_REQ_librelp = "ld-linux-aarch64.so.1 libc.so.6 libgnutls.so.30"
RDEPENDS_librelp = "bash glibc gnutls"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/librelp-1.2.16-1.el8.aarch64.rpm \
          "

SRC_URI[librelp.sha256sum] = "c41b13d2d3a6ab70dbdd4a82d7de55ecf3180fac891da2ade78ea537bd7551a6"
