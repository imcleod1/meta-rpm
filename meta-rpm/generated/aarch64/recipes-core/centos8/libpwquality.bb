SUMMARY = "generated recipe based on libpwquality srpm"
DESCRIPTION = "Description"
LICENSE = "BSD | GPL-2.0"
RPM_LICENSE = "BSD or GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "cracklib pam pkgconfig-native platform-python3"
RPM_SONAME_PROV_libpwquality = "libpwquality.so.1"
RPM_SONAME_REQ_libpwquality = "ld-linux-aarch64.so.1 libc.so.6 libcrack.so.2 libpam.so.0"
RDEPENDS_libpwquality = "cracklib glibc pam"
RPM_SONAME_REQ_libpwquality-devel = "libpwquality.so.1"
RPROVIDES_libpwquality-devel = "libpwquality-dev (= 1.4.0)"
RDEPENDS_libpwquality-devel = "libpwquality pkgconf-pkg-config"
RPM_SONAME_REQ_python3-pwquality = "ld-linux-aarch64.so.1 libc.so.6 libpthread.so.0 libpwquality.so.1 libpython3.6m.so.1.0"
RDEPENDS_python3-pwquality = "glibc libpwquality platform-python python3-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libpwquality-1.4.0-9.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-pwquality-1.4.0-9.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libpwquality-devel-1.4.0-9.el8.aarch64.rpm \
          "

SRC_URI[libpwquality.sha256sum] = "a90e9486e7956f07d1e48a60128c6fcdce7ba45deff90b403e8b77a8d6d77a12"
SRC_URI[libpwquality-devel.sha256sum] = "1e9d9ab18cddc5a5e00a1e0eb4e9e343d5c09c648f75c683a05eca89db8fae32"
SRC_URI[python3-pwquality.sha256sum] = "230bad2a5c2d4863c971290335fd78a6147183db0f5dd34c7c1895b30804c88a"
