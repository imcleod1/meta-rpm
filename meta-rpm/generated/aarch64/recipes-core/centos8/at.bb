SUMMARY = "generated recipe based on at srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0 & GPL-2.0 & ISC & MIT & CLOSED"
RPM_LICENSE = "GPLv3+ and GPLv2+ and ISC and MIT and Public Domain"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libselinux pam pkgconfig-native"
RPM_SONAME_REQ_at = "ld-linux-aarch64.so.1 libc.so.6 libpam.so.0 libpam_misc.so.0 librt.so.1 libselinux.so.1"
RDEPENDS_at = "bash glibc libselinux pam systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/at-3.1.20-11.el8.aarch64.rpm \
          "

SRC_URI[at.sha256sum] = "4f7f237167fc6be711e7254c23859843183ddb744160d8eb7955e02f9dd1fbd1"
