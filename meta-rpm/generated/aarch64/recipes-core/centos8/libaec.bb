SUMMARY = "generated recipe based on libaec srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libaec = "libaec.so.0 libsz.so.2"
RPM_SONAME_REQ_libaec = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libaec = "glibc"
RPM_SONAME_REQ_libaec-devel = "libaec.so.0 libsz.so.2"
RPROVIDES_libaec-devel = "libaec-dev (= 1.0.2)"
RDEPENDS_libaec-devel = "libaec"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libaec-1.0.2-3.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libaec-devel-1.0.2-3.el8.aarch64.rpm \
          "

SRC_URI[libaec.sha256sum] = "189a10391b236744115a90f00da58f96cb9b97f92f1f66bcf726c67a1316bdad"
SRC_URI[libaec-devel.sha256sum] = "6d82886e2255ae83fe67f2de4d09f8db436f35eef85356183ee412bfe125cb20"
