SUMMARY = "generated recipe based on rpm srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
DEPENDS = "acl audit-libs bzip2 db dbus-libs elfutils libcap libselinux lua openssl pkgconfig-native popt xz zlib zstd"
RPM_SONAME_REQ_rpm-plugin-ima = "ld-linux-aarch64.so.1 libacl.so.1 libaudit.so.1 libbz2.so.1 libc.so.6 libcap.so.2 libcrypto.so.1.1 libdb-5.3.so libdl.so.2 libelf.so.1 liblua-5.3.so liblzma.so.5 libm.so.6 libpopt.so.0 libpthread.so.0 librpm.so.8 librpmio.so.8 libz.so.1 libzstd.so.1"
RDEPENDS_rpm-plugin-ima = "audit-libs bzip2-libs elfutils-libelf glibc libacl libcap libdb libzstd lua-libs openssl-libs popt rpm-libs xz-libs zlib"
RPM_SONAME_REQ_rpm-plugin-prioreset = "libacl.so.1 libaudit.so.1 libbz2.so.1 libc.so.6 libcap.so.2 libcrypto.so.1.1 libdb-5.3.so libdl.so.2 libelf.so.1 liblua-5.3.so liblzma.so.5 libm.so.6 libpopt.so.0 libpthread.so.0 librpm.so.8 librpmio.so.8 libz.so.1 libzstd.so.1"
RDEPENDS_rpm-plugin-prioreset = "audit-libs bzip2-libs elfutils-libelf glibc libacl libcap libdb libzstd lua-libs openssl-libs popt rpm-libs xz-libs zlib"
RPM_SONAME_REQ_rpm-plugin-selinux = "ld-linux-aarch64.so.1 libacl.so.1 libaudit.so.1 libbz2.so.1 libc.so.6 libcap.so.2 libcrypto.so.1.1 libdb-5.3.so libdl.so.2 libelf.so.1 liblua-5.3.so liblzma.so.5 libm.so.6 libpopt.so.0 libpthread.so.0 librpm.so.8 librpmio.so.8 libselinux.so.1 libz.so.1 libzstd.so.1"
RDEPENDS_rpm-plugin-selinux = "audit-libs bzip2-libs elfutils-libelf glibc libacl libcap libdb libselinux libzstd lua-libs openssl-libs popt rpm-libs selinux-policy-minimum xz-libs zlib"
RPM_SONAME_REQ_rpm-plugin-syslog = "libacl.so.1 libaudit.so.1 libbz2.so.1 libc.so.6 libcap.so.2 libcrypto.so.1.1 libdb-5.3.so libdl.so.2 libelf.so.1 liblua-5.3.so liblzma.so.5 libm.so.6 libpopt.so.0 libpthread.so.0 librpm.so.8 librpmio.so.8 libz.so.1 libzstd.so.1"
RDEPENDS_rpm-plugin-syslog = "audit-libs bzip2-libs elfutils-libelf glibc libacl libcap libdb libzstd lua-libs openssl-libs popt rpm-libs xz-libs zlib"
RPM_SONAME_REQ_rpm-plugin-systemd-inhibit = "ld-linux-aarch64.so.1 libacl.so.1 libaudit.so.1 libbz2.so.1 libc.so.6 libcap.so.2 libcrypto.so.1.1 libdb-5.3.so libdbus-1.so.3 libdl.so.2 libelf.so.1 liblua-5.3.so liblzma.so.5 libm.so.6 libpopt.so.0 libpthread.so.0 librpm.so.8 librpmio.so.8 libz.so.1 libzstd.so.1"
RDEPENDS_rpm-plugin-systemd-inhibit = "audit-libs bzip2-libs dbus-libs elfutils-libelf glibc libacl libcap libdb libzstd lua-libs openssl-libs popt rpm-libs xz-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/rpm-plugin-ima-4.14.2-37.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/rpm-plugin-prioreset-4.14.2-37.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/rpm-plugin-selinux-4.14.2-37.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/rpm-plugin-syslog-4.14.2-37.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/rpm-plugin-systemd-inhibit-4.14.2-37.el8.aarch64.rpm \
          "

SRC_URI[rpm-plugin-ima.sha256sum] = "5fc158c2a94310756a16dc9b15ffeda9fadf273a7222c055631f3e26d3359fb7"
SRC_URI[rpm-plugin-prioreset.sha256sum] = "8b14ce8d8f3c43d7e26e477033411a886dd4a5f163ee0269e856722c142ad9c5"
SRC_URI[rpm-plugin-selinux.sha256sum] = "5c54f7e5e69ad46bbb096b233c3af6451324befd6884782bd4c544abe768d416"
SRC_URI[rpm-plugin-syslog.sha256sum] = "c62ba833cb2baaaf46826d14b6e166ff066940aa625c3938345de77c71badbc1"
SRC_URI[rpm-plugin-systemd-inhibit.sha256sum] = "7ef699b7d3929cc3b8fa3d593198947fd9eb4cd81e8868ccae7f3b1360ef5eae"
