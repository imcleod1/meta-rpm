SUMMARY = "generated recipe based on rsync srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "acl attr pkgconfig-native popt"
RPM_SONAME_REQ_rsync = "ld-linux-aarch64.so.1 libacl.so.1 libattr.so.1 libc.so.6 libpopt.so.0"
RDEPENDS_rsync = "glibc libacl libattr popt"
RDEPENDS_rsync-daemon = "bash rsync systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/rsync-3.1.3-7.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/rsync-daemon-3.1.3-7.el8.noarch.rpm \
          "

SRC_URI[rsync.sha256sum] = "343f50a183dc28a05319186c5e618c7d34dd70428c95d21ac5017707dc0dc059"
SRC_URI[rsync-daemon.sha256sum] = "03bbac04acad4b1a45b3f8a3c33045df5a3804a0ee3be2b2f04d79e598d14b4e"
