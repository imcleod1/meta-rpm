SUMMARY = "generated recipe based on perl-SUPER srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-SUPER = "perl-Carp perl-Scalar-List-Utils perl-Sub-Identify perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-SUPER-1.20141117-10.el8.noarch.rpm \
          "

SRC_URI[perl-SUPER.sha256sum] = "216cf14692a627097f595e48fe3606a4335bee8ee09e7554e2bc10edd9d118d4"
