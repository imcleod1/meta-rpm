SUMMARY = "generated recipe based on crash-gcore-command srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_crash-gcore-command = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_crash-gcore-command = "crash glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/crash-gcore-command-1.3.1-4.el8.aarch64.rpm \
          "

SRC_URI[crash-gcore-command.sha256sum] = "7ddc6a132ae1811d78fad6e1918c5b81fdfb9baa64f083107d5690e33333d3e8"
