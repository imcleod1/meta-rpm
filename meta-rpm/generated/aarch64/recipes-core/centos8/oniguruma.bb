SUMMARY = "generated recipe based on oniguruma srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_oniguruma = "libonig.so.5"
RPM_SONAME_REQ_oniguruma = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_oniguruma = "glibc"
RPM_SONAME_REQ_oniguruma-devel = "libonig.so.5"
RPROVIDES_oniguruma-devel = "oniguruma-dev (= 6.8.2)"
RDEPENDS_oniguruma-devel = "bash oniguruma pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/oniguruma-6.8.2-1.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/oniguruma-devel-6.8.2-1.el8.aarch64.rpm \
          "

SRC_URI[oniguruma.sha256sum] = "0eb4480fa9490ca0482b886af6a27a88fc921db12a48fa0fb31b85068dead4bf"
SRC_URI[oniguruma-devel.sha256sum] = "b4dca35badd02dc67351bf02edec684b7f548e929d958343338a874ea5786b98"
