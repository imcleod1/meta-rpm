SUMMARY = "generated recipe based on openslp srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "openssl pkgconfig-native zlib"
RPM_SONAME_PROV_openslp = "libslp.so.1"
RPM_SONAME_REQ_openslp = "ld-linux-aarch64.so.1 libc.so.6 libcrypto.so.1.1 libdl.so.2 libm.so.6 libpthread.so.0 libresolv.so.2 libz.so.1"
RDEPENDS_openslp = "glibc openssl-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/openslp-2.0.0-18.el8.aarch64.rpm \
          "

SRC_URI[openslp.sha256sum] = "26d9d954623683368b036eecfe7484b4ba97581fc4e86547fc9ed59aca39ef2c"
