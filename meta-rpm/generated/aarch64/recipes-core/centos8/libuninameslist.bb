SUMMARY = "generated recipe based on libuninameslist srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libuninameslist = "libuninameslist.so.1"
RPM_SONAME_REQ_libuninameslist = "libc.so.6"
RDEPENDS_libuninameslist = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libuninameslist-20170701-4.el8.aarch64.rpm \
          "

SRC_URI[libuninameslist.sha256sum] = "c70eacfa5fe80a90efa4b92685653c111117656ad0fedcd95a273386fff4a28d"
