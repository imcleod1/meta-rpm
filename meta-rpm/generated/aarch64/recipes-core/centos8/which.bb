SUMMARY = "generated recipe based on which srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_which = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_which = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/which-2.21-12.el8.aarch64.rpm \
          "

SRC_URI[which.sha256sum] = "1b18f97ab111e6de0b29e610094a16440c2b6e24a8c47460c4353efc02656bbb"
