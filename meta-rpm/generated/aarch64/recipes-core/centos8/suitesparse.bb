SUMMARY = "generated recipe based on suitesparse srpm"
DESCRIPTION = "Description"
LICENSE = "(LGPL-2.0 | BSD) & LGPL-2.0 & GPL-2.0"
RPM_LICENSE = "(LGPLv2+ or BSD) and LGPLv2+ and GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "atlas libgcc pkgconfig-native tbb"
RPM_SONAME_PROV_suitesparse = "libamd.so.2 libbtf.so.1 libcamd.so.2 libccolamd.so.2 libcholmod.so.3 libcolamd.so.2 libcxsparse.so.3 libklu.so.1 libldl.so.2 librbio.so.2 libspqr.so.2 libsuitesparseconfig.so.4 libumfpack.so.5"
RPM_SONAME_REQ_suitesparse = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libm.so.6 libsatlas.so.3 libstdc++.so.6 libtbb.so.2"
RDEPENDS_suitesparse = "atlas glibc libgcc libstdc++ tbb"
RPM_SONAME_REQ_suitesparse-devel = "libamd.so.2 libbtf.so.1 libcamd.so.2 libccolamd.so.2 libcholmod.so.3 libcolamd.so.2 libcxsparse.so.3 libklu.so.1 libldl.so.2 librbio.so.2 libspqr.so.2 libsuitesparseconfig.so.4 libumfpack.so.5"
RPROVIDES_suitesparse-devel = "suitesparse-dev (= 4.4.6)"
RDEPENDS_suitesparse-devel = "suitesparse"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/suitesparse-4.4.6-11.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/suitesparse-devel-4.4.6-11.el8.aarch64.rpm \
          "

SRC_URI[suitesparse.sha256sum] = "d6fdd9734350129e19834cd41f396e023b52d65847997ba9d278ce4cfb3e0df8"
SRC_URI[suitesparse-devel.sha256sum] = "83ab2821ddc51a2992ba9353d655d9e1e83ff91431f5e8b832d736f357bddb9b"
