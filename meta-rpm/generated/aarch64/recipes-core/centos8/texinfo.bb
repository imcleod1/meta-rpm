SUMMARY = "generated recipe based on texinfo srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "ncurses pkgconfig-native zlib"
RPM_SONAME_REQ_info = "ld-linux-aarch64.so.1 libc.so.6 libtinfo.so.6 libz.so.1"
RDEPENDS_info = "bash glibc ncurses-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/info-6.5-6.el8.aarch64.rpm \
          "

SRC_URI[info.sha256sum] = "187a1fbb7e2992dfa777c7ca5c2f7369ecb85e4be4a483e6c0c6036e02bacf95"
