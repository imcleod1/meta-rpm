SUMMARY = "generated recipe based on perl-Import-Into srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Import-Into = "perl-Module-Runtime perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Import-Into-1.002005-7.el8.noarch.rpm \
          "

SRC_URI[perl-Import-Into.sha256sum] = "69735be04a11806b1e11d216cbfd114e7c1e449ba510234e1c289ef61c882888"
