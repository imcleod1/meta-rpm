SUMMARY = "generated recipe based on glibc srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & LGPL-2.0 & GPL-2.0 & GPL-2.0 & BSD & CLOSED & ISC & CLOSED & GFDL-1.1"
RPM_LICENSE = "LGPLv2+ and LGPLv2+ with exceptions and GPLv2+ and GPLv2+ with exceptions and BSD and Inner-Net and ISC and Public Domain and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPROVIDES_glibc-langpack-nds = "locale-base-nds-de (= 2.28) locale-base-nds-nl (= 2.28) virtual-locale-nds (= 2.28) virtual-locale-nds (= 2.28) virtual-locale-nds-de (= 2.28) virtual-locale-nds-nl (= 2.28)"
RDEPENDS_glibc-langpack-nds = "glibc glibc-common"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-nds-2.28-101.el8.aarch64.rpm \
          "

SRC_URI[glibc-langpack-nds.sha256sum] = "e7940b357d1aa83c9ceb197b73dc6693c0832984a2e4bd30e0dcddbd335d608c"
