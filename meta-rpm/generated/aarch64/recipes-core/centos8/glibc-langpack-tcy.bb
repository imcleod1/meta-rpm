SUMMARY = "generated recipe based on glibc srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & LGPL-2.0 & GPL-2.0 & GPL-2.0 & BSD & CLOSED & ISC & CLOSED & GFDL-1.1"
RPM_LICENSE = "LGPLv2+ and LGPLv2+ with exceptions and GPLv2+ and GPLv2+ with exceptions and BSD and Inner-Net and ISC and Public Domain and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPROVIDES_glibc-langpack-tcy = "locale-base-tcy-in.utf8 (= 2.28) virtual-locale-tcy (= 2.28) virtual-locale-tcy-in.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-tcy = "glibc glibc-common"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-tcy-2.28-101.el8.aarch64.rpm \
          "

SRC_URI[glibc-langpack-tcy.sha256sum] = "3a0d7ef3849ea42dc77ff8bdc60ddcd96387479f680f7a59fdb7b62ed462fb2e"
