SUMMARY = "generated recipe based on virt-what srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_virt-what = "libc.so.6"
RDEPENDS_virt-what = "bash dmidecode glibc util-linux which"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/virt-what-1.18-6.el8.aarch64.rpm \
          "

SRC_URI[virt-what.sha256sum] = "ba9e4f58aab3c5943f5674ca9f9c8fd442f02460ecf1a2ea5beb80229027da8f"
