SUMMARY = "generated recipe based on xorg-x11-drv-fbdev srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_xorg-x11-drv-fbdev = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_xorg-x11-drv-fbdev = "glibc xorg-x11-server-Xorg"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/xorg-x11-drv-fbdev-0.5.0-2.el8.aarch64.rpm \
          "

SRC_URI[xorg-x11-drv-fbdev.sha256sum] = "2d96af094d649cb30c3d5f8177991120cf05ece0635511c32b8a2091282ddb4c"
