SUMMARY = "generated recipe based on setserial srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPL+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_setserial = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_setserial = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/setserial-2.17-45.el8.aarch64.rpm \
          "

SRC_URI[setserial.sha256sum] = "84ced85845b744ac9af92d119f86ac71cb4bb2c33c6c483057e15bdb4efed91e"
