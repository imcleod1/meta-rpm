SUMMARY = "generated recipe based on perl-DateTime-TimeZone-SystemV srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-DateTime-TimeZone-SystemV = "perl-Carp perl-Date-ISO8601 perl-Params-Classify perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-DateTime-TimeZone-SystemV-0.010-3.el8.noarch.rpm \
          "

SRC_URI[perl-DateTime-TimeZone-SystemV.sha256sum] = "45c02e8ed9faea64766be961999308de2fe7decb5b905731f968ac024b165318"
