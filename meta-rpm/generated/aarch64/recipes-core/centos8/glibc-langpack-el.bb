SUMMARY = "generated recipe based on glibc srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & LGPL-2.0 & GPL-2.0 & GPL-2.0 & BSD & CLOSED & ISC & CLOSED & GFDL-1.1"
RPM_LICENSE = "LGPLv2+ and LGPLv2+ with exceptions and GPLv2+ and GPLv2+ with exceptions and BSD and Inner-Net and ISC and Public Domain and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPROVIDES_glibc-langpack-el = "locale-base-el-cy (= 2.28) locale-base-el-cy.utf8 (= 2.28) locale-base-el-gr (= 2.28) locale-base-el-gr.utf8 (= 2.28) locale-base-el-gr@euro (= 2.28) virtual-locale-el (= 2.28) virtual-locale-el (= 2.28) virtual-locale-el (= 2.28) virtual-locale-el (= 2.28) virtual-locale-el (= 2.28) virtual-locale-el-cy (= 2.28) virtual-locale-el-cy.utf8 (= 2.28) virtual-locale-el-gr (= 2.28) virtual-locale-el-gr.utf8 (= 2.28) virtual-locale-el-gr@euro (= 2.28)"
RDEPENDS_glibc-langpack-el = "glibc glibc-common"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/glibc-langpack-el-2.28-101.el8.aarch64.rpm \
          "

SRC_URI[glibc-langpack-el.sha256sum] = "f34adb5232d6117b788846e4910931ddf00c6444960298fb31d01038aeeddf54"
