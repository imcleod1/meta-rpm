SUMMARY = "generated recipe based on perl-Scope-Guard srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Scope-Guard = "perl-Carp perl-Exporter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Scope-Guard-0.21-7.el8.noarch.rpm \
          "

SRC_URI[perl-Scope-Guard.sha256sum] = "1c6d87456b8d630b72f439af93fefb9761b87c491bac7f1fe93bd9bed3718a03"
