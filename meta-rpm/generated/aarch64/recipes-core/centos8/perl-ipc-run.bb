SUMMARY = "generated recipe based on perl-IPC-Run srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-IPC-Run = "perl-Carp perl-Data-Dumper perl-Errno perl-Exporter perl-IO perl-IO-Tty perl-PathTools perl-Scalar-List-Utils perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-IPC-Run-0.99-1.el8.noarch.rpm \
          "

SRC_URI[perl-IPC-Run.sha256sum] = "20865ad642dc00b874b06fc75def97389168c41f054874b2334506b56a45a997"
