SUMMARY = "generated recipe based on hwdata srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/hwdata-0.314-8.4.el8.noarch.rpm \
          "

SRC_URI[hwdata.sha256sum] = "d9d1b63657444369901988eeb8154e0e254b026bbafaf1aba36d78d712d4b6de"
