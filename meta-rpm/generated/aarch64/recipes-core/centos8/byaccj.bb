SUMMARY = "generated recipe based on byaccj srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "Public Domain"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_byaccj = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_byaccj = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/byaccj-1.15-17.module_el8.0.0+30+832da3a1.aarch64.rpm \
          "

SRC_URI[byaccj.sha256sum] = "f8a0534b3c69b71ff89231c2e940b9f74fd99d4978a72a7fc65ecd950e2745ad"
