SUMMARY = "generated recipe based on hunspell-haw srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-haw = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-haw-0.03-1.el8.noarch.rpm \
          "

SRC_URI[hunspell-haw.sha256sum] = "f6dcaa865c2a509b9da29a47c00eccbe21137865d095a909b029fdbdd4b04b90"
