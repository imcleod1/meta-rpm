SUMMARY = "generated recipe based on aspell-en srpm"
DESCRIPTION = "Description"
LICENSE = "MIT & BSD"
RPM_LICENSE = "MIT and BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_aspell-en = "aspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/aspell-en-2017.08.24-2.el8.aarch64.rpm \
          "

SRC_URI[aspell-en.sha256sum] = "cd93d30dfdf3cb4ee77cb39843083937c18bd76a0bdd850b85eaf694fbb323d0"
