SUMMARY = "generated recipe based on perl-Test-NoWarnings srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Test-NoWarnings = "perl-Carp perl-Exporter perl-Test-Simple perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Test-NoWarnings-1.04-15.el8.noarch.rpm \
          "

SRC_URI[perl-Test-NoWarnings.sha256sum] = "c9b381ce47e6b0427995c6627531ed46af3eb4a32b85706f5503c02d255ef81b"
