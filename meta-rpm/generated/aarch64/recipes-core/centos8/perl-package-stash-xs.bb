SUMMARY = "generated recipe based on perl-Package-Stash-XS srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Package-Stash-XS = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Package-Stash-XS = "glibc perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-Package-Stash-XS-0.28-17.el8.aarch64.rpm \
          "

SRC_URI[perl-Package-Stash-XS.sha256sum] = "494f939f072c85588335ded1b0142c5fd9887195ca0b53b82c79999066c3c191"
