SUMMARY = "generated recipe based on diffutils srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_diffutils = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_diffutils = "bash glibc info"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/diffutils-3.6-6.el8.aarch64.rpm \
          "

SRC_URI[diffutils.sha256sum] = "8cbebc0fa970ceca4f479ee292eaad155084987be2cf7f97bbafe4a529319c98"
