SUMMARY = "generated recipe based on xdelta srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native xz"
RPM_SONAME_REQ_xdelta = "ld-linux-aarch64.so.1 libc.so.6 liblzma.so.5 libm.so.6"
RDEPENDS_xdelta = "glibc xz-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/xdelta-3.1.0-4.el8.aarch64.rpm \
          "

SRC_URI[xdelta.sha256sum] = "a3e0b191467ca44d2a0416244f2ae6a4f6e8989ebe16fa982ef7e575425ca59b"
