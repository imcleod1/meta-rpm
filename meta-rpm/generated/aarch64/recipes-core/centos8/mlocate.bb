SUMMARY = "generated recipe based on mlocate srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_mlocate = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_mlocate = "bash glibc grep sed shadow-utils systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/mlocate-0.26-20.el8.aarch64.rpm \
          "

SRC_URI[mlocate.sha256sum] = "a808c5c2faa66331100a87220808a97e6b8e7fc7d13a08dfe8f51d9ad5a33b47"
