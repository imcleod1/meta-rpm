SUMMARY = "generated recipe based on cscope srpm"
DESCRIPTION = "Description"
LICENSE = "BSD & GPL-2.0"
RPM_LICENSE = "BSD and GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "ncurses pkgconfig-native"
RPM_SONAME_REQ_cscope = "ld-linux-aarch64.so.1 libc.so.6 libncurses.so.6 libtinfo.so.6"
RDEPENDS_cscope = "bash emacs-filesystem glibc ncurses-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/cscope-15.9-6.el8.aarch64.rpm \
          "

SRC_URI[cscope.sha256sum] = "efa28c40d6da05f1695a28190c1ac2476c4ad1f807254ba1bb505910c8ddb9c3"
