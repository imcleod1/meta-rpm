SUMMARY = "generated recipe based on numad srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_numad = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6 libpthread.so.0 librt.so.1"
RDEPENDS_numad = "bash glibc systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/numad-0.5-26.20150602git.el8.aarch64.rpm \
          "

SRC_URI[numad.sha256sum] = "5b580f1a1c2193384a7c4c5171200d1e6f4ca6a19e6a01a327a75d03db916484"
