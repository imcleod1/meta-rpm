SUMMARY = "generated recipe based on twolame srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_twolame-libs = "libtwolame.so.0"
RPM_SONAME_REQ_twolame-libs = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6"
RDEPENDS_twolame-libs = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/twolame-libs-0.3.13-11.el8.aarch64.rpm \
          "

SRC_URI[twolame-libs.sha256sum] = "94f361c7b3a3dae83444d007aefe4ef9b211733171d2157b4c45de1a2b3565ed"
