SUMMARY = "generated recipe based on gmp srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-3.0 | GPL-2.0"
RPM_LICENSE = "LGPLv3+ or GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_PROV_gmp = "libgmp.so.10"
RPM_SONAME_REQ_gmp = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_gmp = "glibc"
RPM_SONAME_PROV_gmp-c++ = "libgmpxx.so.4"
RPM_SONAME_REQ_gmp-c++ = "ld-linux-aarch64.so.1 libc.so.6 libgcc_s.so.1 libgmp.so.10 libstdc++.so.6"
RDEPENDS_gmp-c++ = "glibc gmp libgcc libstdc++"
RPM_SONAME_REQ_gmp-devel = "libgmp.so.10 libgmpxx.so.4"
RPROVIDES_gmp-devel = "gmp-dev (= 6.1.2)"
RDEPENDS_gmp-devel = "bash gmp gmp-c++ info"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/gmp-6.1.2-10.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/gmp-c++-6.1.2-10.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/gmp-devel-6.1.2-10.el8.aarch64.rpm \
          "

SRC_URI[gmp.sha256sum] = "8d407f8ad961169fca2ee5e22e824cbc2d2b5fedca9701896cc492d4cb788603"
SRC_URI[gmp-c++.sha256sum] = "138d30b2c2ccbaf7a06cba8d7d9d1cec08cc6405ca8b790640facc3bfd17a3f6"
SRC_URI[gmp-devel.sha256sum] = "945b1ef538be69fbc7d7836b344aa7a623de2f1cd4e6724b4ef89dacaed0e376"
