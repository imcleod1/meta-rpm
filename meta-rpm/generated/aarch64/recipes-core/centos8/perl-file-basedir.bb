SUMMARY = "generated recipe based on perl-File-BaseDir srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-File-BaseDir = "perl-Carp perl-Exporter perl-PathTools perl-libs perl-parent"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/perl-File-BaseDir-0.08-1.el8.noarch.rpm \
          "

SRC_URI[perl-File-BaseDir.sha256sum] = "71a9977ad15e1a877ee6bcc8c367fe4a5e03e1991c2956cf0df61a4579ea36d0"
