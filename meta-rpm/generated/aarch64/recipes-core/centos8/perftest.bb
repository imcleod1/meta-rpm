SUMMARY = "generated recipe based on perftest srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | BSD"
RPM_LICENSE = "GPLv2 or BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native rdma-core"
RPM_SONAME_REQ_perftest = "ld-linux-aarch64.so.1 libc.so.6 libibumad.so.3 libibverbs.so.1 libm.so.6 libpthread.so.0 librdmacm.so.1"
RDEPENDS_perftest = "glibc libibumad libibverbs librdmacm"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/perftest-4.2-2.el8.aarch64.rpm \
          "

SRC_URI[perftest.sha256sum] = "cad2754bf7ad737358f7662847cf8950a22f91fdf35118008dd73db0fc6f919f"
