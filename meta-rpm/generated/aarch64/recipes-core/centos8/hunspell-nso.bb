SUMMARY = "generated recipe based on hunspell-nso srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-nso = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-nso-0.20091201-14.el8.noarch.rpm \
          "

SRC_URI[hunspell-nso.sha256sum] = "32665f6ed87b5f2ddcd6e0b67d5a5337ed2c9d3846b94856b07ffc5ce3e31d89"
