SUMMARY = "generated recipe based on wavpack srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_wavpack = "libwavpack.so.1"
RPM_SONAME_REQ_wavpack = "ld-linux-aarch64.so.1 libc.so.6 libm.so.6"
RDEPENDS_wavpack = "glibc"
RPM_SONAME_REQ_wavpack-devel = "libwavpack.so.1"
RPROVIDES_wavpack-devel = "wavpack-dev (= 5.1.0)"
RDEPENDS_wavpack-devel = "pkgconf-pkg-config wavpack"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/wavpack-5.1.0-15.el8.aarch64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/wavpack-devel-5.1.0-15.el8.aarch64.rpm \
          "

SRC_URI[wavpack.sha256sum] = "2af2a611a4ba8237c3521058d29ad65a2c6207ac970cee438c8eb98bd711b684"
SRC_URI[wavpack-devel.sha256sum] = "0c331a6c50ace9dd94507bbe597a65bc33557bff35dd0c2f63301aadbd69dac0"
