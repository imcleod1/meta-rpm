SUMMARY = "generated recipe based on usb_modeswitch srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libusb1 pkgconfig-native"
RPM_SONAME_REQ_usb_modeswitch = "ld-linux-aarch64.so.1 libc.so.6 libusb-1.0.so.0"
RDEPENDS_usb_modeswitch = "bash glibc jimtcl libusbx systemd usb_modeswitch-data"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/usb_modeswitch-2.5.2-1.el8.aarch64.rpm \
          "

SRC_URI[usb_modeswitch.sha256sum] = "04af63a1f1cf5d0428d83e3a92b36d83da594e5910e5499f1744bc6ad46aeb37"
