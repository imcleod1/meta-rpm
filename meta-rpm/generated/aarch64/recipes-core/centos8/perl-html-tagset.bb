SUMMARY = "generated recipe based on perl-HTML-Tagset srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-HTML-Tagset = "perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-HTML-Tagset-3.20-33.el8.noarch.rpm \
          "

SRC_URI[perl-HTML-Tagset.sha256sum] = "f6aee119c49401cf04167de20a4284ed0f890d3ce12b88d3231849f047ee9b87"
