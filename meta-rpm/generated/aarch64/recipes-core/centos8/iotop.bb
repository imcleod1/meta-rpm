SUMMARY = "generated recipe based on iotop srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_iotop = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/iotop-0.6-16.el8.noarch.rpm \
          "

SRC_URI[iotop.sha256sum] = "b8c4dae9af478df8c501f099bdd98d0140df59c45d3f734b8031fa7451366158"
