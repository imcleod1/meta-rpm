SUMMARY = "generated recipe based on hunspell-ru srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-ru = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-ru-0.99g5-13.el8.noarch.rpm \
          "

SRC_URI[hunspell-ru.sha256sum] = "0a48c4ce4fa777373bf1e83867e46bc983152a3c1b5cd480a3c715a09c43dcea"
