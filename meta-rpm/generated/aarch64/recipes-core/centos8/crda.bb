SUMMARY = "generated recipe based on crda srpm"
DESCRIPTION = "Description"
LICENSE = "ISC"
RPM_LICENSE = "ISC"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_crda = "bash iw kernel systemd systemd-udev"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/crda-3.18_2018.05.31-3.el8.noarch.rpm \
          "

SRC_URI[crda.sha256sum] = "d2d436ffdf509c92850dd2599f25429014200fd74dad2c1517b7872467696391"
