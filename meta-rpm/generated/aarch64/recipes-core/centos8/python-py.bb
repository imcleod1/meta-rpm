SUMMARY = "generated recipe based on python-py srpm"
DESCRIPTION = "Description"
LICENSE = "MIT & CLOSED"
RPM_LICENSE = "MIT and Public Domain"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-py = "platform-python platform-python-setuptools"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-py-1.5.3-4.el8.noarch.rpm \
          "

SRC_URI[python3-py.sha256sum] = "1f1d5883ad101c202c483d6dda638d728f1245697753bde9e73c701c8c5acefb"
