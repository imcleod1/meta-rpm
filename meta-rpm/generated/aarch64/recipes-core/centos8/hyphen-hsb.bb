SUMMARY = "generated recipe based on hyphen-hsb srpm"
DESCRIPTION = "Description"
LICENSE = "LPPL-1.0"
RPM_LICENSE = "LPPL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-hsb = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hyphen-hsb-0.20110620-12.el8.noarch.rpm \
          "

SRC_URI[hyphen-hsb.sha256sum] = "43c11949a9e8ba9d8afdace391114886d1bc6caf238cac55bca155e034a2bfad"
