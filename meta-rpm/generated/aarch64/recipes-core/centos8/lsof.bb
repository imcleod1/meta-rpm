SUMMARY = "generated recipe based on lsof srpm"
DESCRIPTION = "Description"
LICENSE = "Zlib & CLOSED & LGPL-2.0"
RPM_LICENSE = "zlib and Sendmail and LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libselinux libtirpc pkgconfig-native"
RPM_SONAME_REQ_lsof = "ld-linux-aarch64.so.1 libc.so.6 libselinux.so.1 libtirpc.so.3"
RDEPENDS_lsof = "glibc libselinux libtirpc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/lsof-4.91-2.el8.aarch64.rpm \
          "

SRC_URI[lsof.sha256sum] = "a76d087ed173984e1318a778165c7d4ee4339858642dc1bc90be41cd578f9a53"
