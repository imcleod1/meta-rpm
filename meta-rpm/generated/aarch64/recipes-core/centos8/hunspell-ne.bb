SUMMARY = "generated recipe based on hunspell-ne srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-ne = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/hunspell-ne-20080425-17.el8.noarch.rpm \
          "

SRC_URI[hunspell-ne.sha256sum] = "3d4c758c72ea711a2f2a6186bbeb76b806a73ec2e656d90a19426ad49ad22853"
