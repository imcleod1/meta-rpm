SUMMARY = "generated recipe based on go-compilers srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/go-compilers-golang-compiler-1-20.el8.aarch64.rpm \
          "

SRC_URI[go-compilers-golang-compiler.sha256sum] = "713e525ee3073b1f3168f400cc7e4b3ee109afb99445eba6f16d3366ff0d3f05"
