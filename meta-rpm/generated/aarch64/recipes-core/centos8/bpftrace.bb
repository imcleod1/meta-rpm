SUMMARY = "generated recipe based on bpftrace srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "bcc clang elfutils libgcc llvm pkgconfig-native"
RPM_SONAME_REQ_bpftrace = "ld-linux-aarch64.so.1 libLLVM-9.so libbcc.so.0 libc.so.6 libclang.so.9 libelf.so.1 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_bpftrace = "bcc clang-libs elfutils-libelf glibc libgcc libstdc++ llvm-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/bpftrace-0.9.2-1.el8.aarch64.rpm \
          "

SRC_URI[bpftrace.sha256sum] = "7f1e83159fbd120bcdcb00bb241154d697db6f22d0bc2494f33a6fbc562ae320"
