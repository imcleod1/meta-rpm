SUMMARY = "generated recipe based on libtool srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libtool-ltdl = "libltdl.so.7"
RPM_SONAME_REQ_libtool-ltdl = "ld-linux-aarch64.so.1 libc.so.6 libdl.so.2"
RDEPENDS_libtool-ltdl = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/libtool-ltdl-2.4.6-25.el8.aarch64.rpm \
          "

SRC_URI[libtool-ltdl.sha256sum] = "0b45f752d477abd5e83d2c5966891b41a042e5f74c1e7c503a42ff5aba231c9b"
