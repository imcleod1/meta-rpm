SUMMARY = "generated recipe based on python-jsonpointer srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-jsonpointer = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/python3-jsonpointer-1.10-11.el8.noarch.rpm \
          "

SRC_URI[python3-jsonpointer.sha256sum] = "60b0cbc5e435be346bb06f45f3336f8936d7362022d8bceda80ff16bc3fb7dd2"
