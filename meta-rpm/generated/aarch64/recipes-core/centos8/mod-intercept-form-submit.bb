SUMMARY = "generated recipe based on mod_intercept_form_submit srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_mod_intercept_form_submit = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_mod_intercept_form_submit = "glibc httpd mod_authnz_pam"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/mod_intercept_form_submit-1.1.0-5.el8.aarch64.rpm \
          "

SRC_URI[mod_intercept_form_submit.sha256sum] = "93dcf36b06d092cd90a2014e3d2c690a6cddcc75f654f8acb91c4ff6fd81089b"
