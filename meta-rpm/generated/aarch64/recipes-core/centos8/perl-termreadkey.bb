SUMMARY = "generated recipe based on perl-TermReadKey srpm"
DESCRIPTION = "Description"
LICENSE = "(CLOSED) & (Artistic-1.0 | GPL-2.0)"
RPM_LICENSE = "(Copyright only) and (Artistic or GPL+)"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-TermReadKey = "ld-linux-aarch64.so.1 libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-TermReadKey = "glibc perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/aarch64/os/Packages/perl-TermReadKey-2.37-7.el8.aarch64.rpm \
          "

SRC_URI[perl-TermReadKey.sha256sum] = "22c5761a5337e189c654e577ab4a8882969ddb56df8d4bed20a48be21086c58c"
