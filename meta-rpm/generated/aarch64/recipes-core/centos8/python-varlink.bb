SUMMARY = "generated recipe based on python-varlink srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-varlink = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-varlink-29.0.0-1.el8.noarch.rpm \
          "

SRC_URI[python3-varlink.sha256sum] = "67e35b6f0d3aa78ab4c1c349f410d08bf49bfc1cb25857e0477fe6743d0868d1"
