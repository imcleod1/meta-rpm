SUMMARY = "generated recipe based on python-pycparser srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-pycparser = "platform-python python3-ply"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/aarch64/os/Packages/python3-pycparser-2.14-14.el8.noarch.rpm \
          "

SRC_URI[python3-pycparser.sha256sum] = "8891a9a4707611c13a5693b195201dd940254ffdb03cf5742952329282bb8cb7"
