SUMMARY = "generated recipe based on libeot srpm"
DESCRIPTION = "Description"
LICENSE = "MPL-2.0"
RPM_LICENSE = "MPLv2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "aarch64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libeot = "libeot.so.0"
RPM_SONAME_REQ_libeot = "ld-linux-aarch64.so.1 libc.so.6"
RDEPENDS_libeot = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/aarch64/os/Packages/libeot-0.01-9.el8.aarch64.rpm \
          "

SRC_URI[libeot.sha256sum] = "6df5ff536c71e06f570e7d73c9210cac5c2ef4195b480a0968f54569c370f41d"
