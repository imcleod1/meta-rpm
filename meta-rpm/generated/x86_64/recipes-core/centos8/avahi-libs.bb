SUMMARY = "generated recipe based on avahi srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "dbus-libs pkgconfig-native"
RPM_SONAME_REQ_avahi-devel = "libavahi-client.so.3 libavahi-common.so.3 libavahi-core.so.7"
RPROVIDES_avahi-devel = "avahi-dev (= 0.7)"
RDEPENDS_avahi-devel = "avahi avahi-libs pkgconf-pkg-config"
RPM_SONAME_PROV_avahi-libs = "libavahi-client.so.3 libavahi-common.so.3"
RPM_SONAME_REQ_avahi-libs = "libc.so.6 libdbus-1.so.3 libdl.so.2 libpthread.so.0"
RDEPENDS_avahi-libs = "dbus-libs glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/avahi-libs-0.7-19.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/avahi-devel-0.7-19.el8.x86_64.rpm \
          "

SRC_URI[avahi-devel.sha256sum] = "0f39a34c71e3ec3131c81004b5acd1e70a5e4647f6a165bab178297f2009436b"
SRC_URI[avahi-libs.sha256sum] = "72a335db756045d60f7e2767fe7518e60392b6c19592689140c059e1466c34e2"
