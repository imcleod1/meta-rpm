SUMMARY = "generated recipe based on gzip srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0 & GFDL-1.1"
RPM_LICENSE = "GPLv3+ and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_gzip = "libc.so.6"
RDEPENDS_gzip = "bash coreutils glibc info"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/gzip-1.9-9.el8.x86_64.rpm \
          "

SRC_URI[gzip.sha256sum] = "6697e1ec0e3b6eedb1f7c1c25e1e4505926e1de929bb4235304f4a6f9faa5bcb"
