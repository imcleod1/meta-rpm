SUMMARY = "generated recipe based on audit srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libcap-ng pkgconfig-native"
RPM_SONAME_PROV_audit-libs = "libaudit.so.1 libauparse.so.0"
RPM_SONAME_REQ_audit-libs = "libc.so.6 libcap-ng.so.0 libpthread.so.0"
RDEPENDS_audit-libs = "glibc libcap-ng"
RPM_SONAME_REQ_audit-libs-devel = "libaudit.so.1 libauparse.so.0"
RPROVIDES_audit-libs-devel = "audit-libs-dev (= 3.0)"
RDEPENDS_audit-libs-devel = "audit-libs kernel-headers pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/audit-libs-3.0-0.17.20191104git1c2f876.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/audit-libs-devel-3.0-0.17.20191104git1c2f876.el8.x86_64.rpm \
          "

SRC_URI[audit-libs.sha256sum] = "e7da6b155db78fb2015c40663fec6e475a44b21b1c2124496cf23f862e021db8"
SRC_URI[audit-libs-devel.sha256sum] = "2af7afff020d5536ae2153214a6fb2b5a67ed9d264f1a99c1eb23a07db5dfb2e"
