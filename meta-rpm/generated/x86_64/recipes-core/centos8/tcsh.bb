SUMMARY = "generated recipe based on tcsh srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libxcrypt ncurses pkgconfig-native"
RPM_SONAME_REQ_tcsh = "libc.so.6 libcrypt.so.1 libtinfo.so.6"
RDEPENDS_tcsh = "bash coreutils glibc grep libxcrypt ncurses-libs sed"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/tcsh-6.20.00-12.el8.x86_64.rpm \
          "

SRC_URI[tcsh.sha256sum] = "6e1a1fef0c88d148ec793a2af3bde498b91e4435ffb46a9aea5232298a539e80"
