SUMMARY = "generated recipe based on apache-resource-bundles srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_apache-resource-bundles = "java-1.8.0-openjdk-headless javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/apache-resource-bundles-2-20.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[apache-resource-bundles.sha256sum] = "3d36f3a633f54feecae30a4a09aabcf657b935cdbe055c94ac3b60f88c103f08"
