SUMMARY = "generated recipe based on authd srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "openssl pkgconfig-native"
RPM_SONAME_REQ_authd = "libc.so.6 libcrypto.so.1.1"
RDEPENDS_authd = "bash glibc openssl openssl-libs systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/authd-1.4.4-5.el8_0.1.x86_64.rpm \
          "

SRC_URI[authd.sha256sum] = "3e866167a656cf664963964cfa9418e287cd4724246ef35461408d210e5bffc4"
