SUMMARY = "generated recipe based on perl-YAML-Syck srpm"
DESCRIPTION = "Description"
LICENSE = "BSD & MIT"
RPM_LICENSE = "BSD and MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-YAML-Syck = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-YAML-Syck = "glibc perl-Exporter perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-YAML-Syck-1.30-5.el8.x86_64.rpm \
          "

SRC_URI[perl-YAML-Syck.sha256sum] = "18633c16c3f4fec7cfbfe09f47d9990715b758213b68bca330b9f7728119872d"
