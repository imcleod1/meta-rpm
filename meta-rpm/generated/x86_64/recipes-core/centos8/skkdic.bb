SUMMARY = "generated recipe based on skkdic srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/skkdic-20170102-4.T1100.el8.noarch.rpm \
          "

SRC_URI[skkdic.sha256sum] = "114a604321a9661775fd9611763a830f68c36f0eae64abbc9e03d363209c62ca"
