SUMMARY = "generated recipe based on crash-ptdump-command srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_crash-ptdump-command = "libc.so.6"
RDEPENDS_crash-ptdump-command = "crash glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/crash-ptdump-command-1.0.3-5.el8.x86_64.rpm \
          "

SRC_URI[crash-ptdump-command.sha256sum] = "87574e2d45d198928495883e455f150f2b31189f9259012634aebebb81258379"
