SUMMARY = "generated recipe based on policycoreutils srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_policycoreutils-gui = "bash gtk3 platform-python policycoreutils-dbus policycoreutils-devel python3-gobject python3-policycoreutils"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/policycoreutils-gui-2.9-9.el8.noarch.rpm \
          "

SRC_URI[policycoreutils-gui.sha256sum] = "fa3d4862eb1cb53cd83f214d8afd847f2bfe76ad045edf2bd675fb0a46a750df"
