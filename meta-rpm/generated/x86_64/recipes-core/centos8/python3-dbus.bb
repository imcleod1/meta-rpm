SUMMARY = "generated recipe based on dbus-python srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "dbus-glib dbus-libs glib-2.0 pkgconfig-native"
RPM_SONAME_REQ_python3-dbus = "libc.so.6 libdbus-1.so.3 libdbus-glib-1.so.2 libglib-2.0.so.0 libgobject-2.0.so.0"
RDEPENDS_python3-dbus = "dbus-glib dbus-libs glib2 glibc platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-dbus-1.2.4-15.el8.x86_64.rpm \
          "

SRC_URI[python3-dbus.sha256sum] = "066f254f9ac7712b44214816de907a87eb8dfd0d2ea9570a7513db9a6617ba26"
