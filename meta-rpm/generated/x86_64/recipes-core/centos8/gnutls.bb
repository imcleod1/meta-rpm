SUMMARY = "generated recipe based on gnutls srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "autogen gmp gnutls-libs libgcc libidn2 libtasn1 libunistring nettle p11-kit pkgconfig-native unbound"
RPM_SONAME_PROV_gnutls-c++ = "libgnutlsxx.so.28"
RPM_SONAME_REQ_gnutls-c++ = "libc.so.6 libgcc_s.so.1 libgnutls.so.30 libm.so.6 libstdc++.so.6"
RDEPENDS_gnutls-c++ = "glibc gnutls libgcc libstdc++"
RPM_SONAME_PROV_gnutls-dane = "libgnutls-dane.so.0"
RPM_SONAME_REQ_gnutls-dane = "libc.so.6 libgnutls.so.30 libunbound.so.2"
RDEPENDS_gnutls-dane = "glibc gnutls unbound-libs"
RPM_SONAME_REQ_gnutls-devel = "libgnutls-dane.so.0 libgnutls.so.30 libgnutlsxx.so.28"
RPROVIDES_gnutls-devel = "gnutls-dev (= 3.6.8)"
RDEPENDS_gnutls-devel = "bash gnutls gnutls-c++ gnutls-dane info libtasn1-devel nettle-devel p11-kit-devel pkgconf-pkg-config"
RPM_SONAME_REQ_gnutls-utils = "libc.so.6 libdl.so.2 libgmp.so.10 libgnutls-dane.so.0 libgnutls.so.30 libhogweed.so.4 libidn2.so.0 libnettle.so.6 libopts.so.25 libp11-kit.so.0 libtasn1.so.6 libunistring.so.2"
RDEPENDS_gnutls-utils = "autogen-libopts glibc gmp gnutls gnutls-dane libidn2 libtasn1 libunistring nettle p11-kit"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnutls-c++-3.6.8-11.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnutls-dane-3.6.8-11.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnutls-devel-3.6.8-11.el8_2.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gnutls-utils-3.6.8-11.el8_2.x86_64.rpm \
          "

SRC_URI[gnutls-c++.sha256sum] = "5921669290f2f967846b98c8a3814dda1dbbb98df219236340f3bd20f2689f06"
SRC_URI[gnutls-dane.sha256sum] = "91135fed6fd15efe2c8542aed6e4cf2faf29b577930bd49ff77d0e66e6505b88"
SRC_URI[gnutls-devel.sha256sum] = "2ce30d719d2f8b6b539e88dda7e4d01131cbb47f5c39f23e122c15415bb0b93c"
SRC_URI[gnutls-utils.sha256sum] = "31568ba503c562c375821ea3d898e3457b1845acc63ce27d4a9c1ad072b67308"
