SUMMARY = "generated recipe based on perl-Test-Exception srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Test-Exception = "perl-Carp perl-Exporter perl-Sub-Uplevel perl-Test-Simple perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Test-Exception-0.43-7.el8.noarch.rpm \
          "

SRC_URI[perl-Test-Exception.sha256sum] = "6f07d665ad7beef3669840088c06bd81212a7d3d8bf4cc714348d57f54248c71"
