SUMMARY = "generated recipe based on perl-MailTools srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-MailTools = "perl-Carp perl-Exporter perl-IO perl-Net-SMTP-SSL perl-TimeDate perl-interpreter perl-libnet perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-MailTools-2.20-2.el8.noarch.rpm \
          "

SRC_URI[perl-MailTools.sha256sum] = "ffa431832aaf83ec022a4acee61d8172edb9daf385b9708997d1f707c418e800"
