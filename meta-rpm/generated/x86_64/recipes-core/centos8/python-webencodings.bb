SUMMARY = "generated recipe based on python-webencodings srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-webencodings = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-webencodings-0.5.1-6.el8.noarch.rpm \
          "

SRC_URI[python3-webencodings.sha256sum] = "6ee1c354e7a78accc7a1f7495f9ff401181d403e7dc30877cd0a95c5ebb5ef69"
