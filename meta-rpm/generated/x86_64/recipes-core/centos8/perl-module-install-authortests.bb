SUMMARY = "generated recipe based on perl-Module-Install-AuthorTests srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Module-Install-AuthorTests = "perl-Carp perl-Module-Install perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Module-Install-AuthorTests-0.002-16.el8.noarch.rpm \
          "

SRC_URI[perl-Module-Install-AuthorTests.sha256sum] = "31005bd3a129e71badf2d5962ab36243d774dd10592796bea4024742077476a4"
