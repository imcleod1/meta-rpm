SUMMARY = "generated recipe based on time srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0 & GFDL-1.1"
RPM_LICENSE = "GPLv3+ and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_time = "libc.so.6"
RDEPENDS_time = "bash glibc info"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/time-1.9-3.el8.x86_64.rpm \
          "

SRC_URI[time.sha256sum] = "88cf7488620e05f294f5ef0fc417472e9429329f32fb4dda2cf0c51cdf7da172"
