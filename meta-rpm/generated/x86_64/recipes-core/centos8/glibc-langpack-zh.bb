SUMMARY = "generated recipe based on glibc srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & LGPL-2.0 & GPL-2.0 & GPL-2.0 & BSD & CLOSED & ISC & CLOSED & GFDL-1.1"
RPM_LICENSE = "LGPLv2+ and LGPLv2+ with exceptions and GPLv2+ and GPLv2+ with exceptions and BSD and Inner-Net and ISC and Public Domain and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPROVIDES_glibc-langpack-zh = "locale-base-zh-cn (= 2.28) locale-base-zh-cn.gb18030 (= 2.28) locale-base-zh-cn.gbk (= 2.28) locale-base-zh-cn.utf8 (= 2.28) locale-base-zh-hk (= 2.28) locale-base-zh-hk.utf8 (= 2.28) locale-base-zh-sg (= 2.28) locale-base-zh-sg.gbk (= 2.28) locale-base-zh-sg.utf8 (= 2.28) locale-base-zh-tw (= 2.28) locale-base-zh-tw.euctw (= 2.28) locale-base-zh-tw.utf8 (= 2.28) virtual-locale-zh (= 2.28) virtual-locale-zh (= 2.28) virtual-locale-zh (= 2.28) virtual-locale-zh (= 2.28) virtual-locale-zh (= 2.28) virtual-locale-zh (= 2.28) virtual-locale-zh (= 2.28) virtual-locale-zh (= 2.28) virtual-locale-zh (= 2.28) virtual-locale-zh (= 2.28) virtual-locale-zh (= 2.28) virtual-locale-zh (= 2.28) virtual-locale-zh-cn (= 2.28) virtual-locale-zh-cn.gb18030 (= 2.28) virtual-locale-zh-cn.gbk (= 2.28) virtual-locale-zh-cn.utf8 (= 2.28) virtual-locale-zh-hk (= 2.28) virtual-locale-zh-hk.utf8 (= 2.28) virtual-locale-zh-sg (= 2.28) virtual-locale-zh-sg.gbk (= 2.28) virtual-locale-zh-sg.utf8 (= 2.28) virtual-locale-zh-tw (= 2.28) virtual-locale-zh-tw.euctw (= 2.28) virtual-locale-zh-tw.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-zh = "glibc glibc-common"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-zh-2.28-101.el8.x86_64.rpm \
          "

SRC_URI[glibc-langpack-zh.sha256sum] = "cd179c6dbceb5822ffb927d587f0b079e96001fcd08ecffd9ecba184e944faf1"
