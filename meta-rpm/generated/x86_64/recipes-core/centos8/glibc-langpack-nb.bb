SUMMARY = "generated recipe based on glibc srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & LGPL-2.0 & GPL-2.0 & GPL-2.0 & BSD & CLOSED & ISC & CLOSED & GFDL-1.1"
RPM_LICENSE = "LGPLv2+ and LGPLv2+ with exceptions and GPLv2+ and GPLv2+ with exceptions and BSD and Inner-Net and ISC and Public Domain and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPROVIDES_glibc-langpack-nb = "locale-base-nb-no (= 2.28) locale-base-nb-no.utf8 (= 2.28) virtual-locale-nb (= 2.28) virtual-locale-nb (= 2.28) virtual-locale-nb-no (= 2.28) virtual-locale-nb-no.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-nb = "glibc glibc-common"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-nb-2.28-101.el8.x86_64.rpm \
          "

SRC_URI[glibc-langpack-nb.sha256sum] = "2b75048d3a306ae0bb49472f49d94e8f09010f6c702ef50a32bff3266735d0e2"
