SUMMARY = "generated recipe based on perl-NTLM srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-NTLM = "perl-Digest-HMAC perl-Exporter perl-MIME-Base64 perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-NTLM-1.09-17.el8.noarch.rpm \
          "

SRC_URI[perl-NTLM.sha256sum] = "304f32c764d463d88e312018db685217782084a42093492f73e6f9236b938b10"
