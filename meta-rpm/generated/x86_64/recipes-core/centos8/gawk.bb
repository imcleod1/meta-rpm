SUMMARY = "generated recipe based on gawk srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0 & GPL-2.0 & LGPL-2.0 & BSD"
RPM_LICENSE = "GPLv3+ and GPLv2+ and LGPLv2+ and BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "gmp libsigsegv mpfr pkgconfig-native readline"
RPM_SONAME_REQ_gawk = "libc.so.6 libdl.so.2 libgmp.so.10 libm.so.6 libmpfr.so.4 libreadline.so.7 libsigsegv.so.2"
RDEPENDS_gawk = "filesystem glibc gmp libsigsegv mpfr readline"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/gawk-4.2.1-1.el8.x86_64.rpm \
          "

SRC_URI[gawk.sha256sum] = "5daf8c72daefddf2b0392c8bd3d73865b51386a06983dffe6128cf4ba9de7b90"
