SUMMARY = "generated recipe based on perl-Test-Pod srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Test-Pod = "perl-PathTools perl-Pod-Simple perl-Test-Simple perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Test-Pod-1.51-8.el8.noarch.rpm \
          "

SRC_URI[perl-Test-Pod.sha256sum] = "28530415690bc403b710aee74fda336d4d422ffe01b37af30f25820e6f0f7031"
