SUMMARY = "generated recipe based on python3 srpm"
DESCRIPTION = "Description"
LICENSE = "Python-2.0"
RPM_LICENSE = "Python"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libx11 openssl pkgconfig-native tcl tk"
RDEPENDS_python3-idle = "bash platform-python python3-tkinter python36"
RPM_SONAME_REQ_python3-tkinter = "libX11.so.6 libc.so.6 libcrypto.so.1.1 libpthread.so.0 libpython3.6m.so.1.0 libtcl8.6.so libtk8.6.so"
RDEPENDS_python3-tkinter = "glibc libX11 openssl-libs platform-python python3-libs tcl tk"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-idle-3.6.8-23.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-tkinter-3.6.8-23.el8.x86_64.rpm \
          "

SRC_URI[python3-idle.sha256sum] = "e2c8fe9a4ef913c437edc526e115f65b6faca353b23bae353327020f124dcc5b"
SRC_URI[python3-tkinter.sha256sum] = "e621b3e5be690ba6ed5c9109055b0de7040db476056d707b892fdd62be466237"
