SUMMARY = "generated recipe based on audit srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "audit-libs krb5-libs libcap-ng pkgconfig-native"
RPM_SONAME_REQ_audispd-plugins = "libaudit.so.1 libauparse.so.0 libc.so.6 libcap-ng.so.0 libgssapi_krb5.so.2 libkrb5.so.3 libpthread.so.0"
RDEPENDS_audispd-plugins = "audit audit-libs glibc krb5-libs libcap-ng"
RPM_SONAME_REQ_audispd-plugins-zos = "libaudit.so.1 libauparse.so.0 libc.so.6 libcap-ng.so.0 liblber-2.4.so.2 libldap-2.4.so.2 libpthread.so.0"
RDEPENDS_audispd-plugins-zos = "audit audit-libs glibc libcap-ng openldap"
RPM_SONAME_REQ_audit = "libaudit.so.1 libauparse.so.0 libc.so.6 libcap-ng.so.0 libgssapi_krb5.so.2 libkrb5.so.3 libm.so.6 libpthread.so.0 librt.so.1"
RDEPENDS_audit = "audit-libs bash coreutils glibc initscripts krb5-libs libcap-ng systemd"
RPM_SONAME_REQ_python3-audit = "libaudit.so.1 libauparse.so.0 libc.so.6 libcap-ng.so.0 libpthread.so.0"
RDEPENDS_python3-audit = "audit-libs glibc libcap-ng platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/audispd-plugins-3.0-0.17.20191104git1c2f876.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/audispd-plugins-zos-3.0-0.17.20191104git1c2f876.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/audit-3.0-0.17.20191104git1c2f876.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-audit-3.0-0.17.20191104git1c2f876.el8.x86_64.rpm \
          "

SRC_URI[audispd-plugins.sha256sum] = "74f2ef448a2de591ff8ca29c9f84ad96194b978848334247cba64ee416631863"
SRC_URI[audispd-plugins-zos.sha256sum] = "a5a235eba36f1f2687f87050a55a5f821e51fa99f3ae130f56dfa0cf0be5df96"
SRC_URI[audit.sha256sum] = "b0149d85f0172e98866ff2483660af2cf6c6fa0c8f9cab2c51cc2af479c9e319"
SRC_URI[python3-audit.sha256sum] = "addf80c52d794aed47874eb9d5ddbbaa90cb248fda1634d793054a41da0d92d7"
