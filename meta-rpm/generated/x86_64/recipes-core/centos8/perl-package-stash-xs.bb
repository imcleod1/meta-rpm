SUMMARY = "generated recipe based on perl-Package-Stash-XS srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Package-Stash-XS = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Package-Stash-XS = "glibc perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Package-Stash-XS-0.28-17.el8.x86_64.rpm \
          "

SRC_URI[perl-Package-Stash-XS.sha256sum] = "0f0c20937184ceb88d0cc3f3c88518ae262ca8dcee9816880db40651cb434386"
