SUMMARY = "generated recipe based on wpa_supplicant srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "dbus-libs libnl openssl pkgconfig-native"
RPM_SONAME_REQ_wpa_supplicant = "libc.so.6 libcrypto.so.1.1 libdbus-1.so.3 libdl.so.2 libnl-3.so.200 libnl-genl-3.so.200 libnl-route-3.so.200 librt.so.1 libssl.so.1.1"
RDEPENDS_wpa_supplicant = "bash dbus-libs glibc libnl3 openssl-libs systemd"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/wpa_supplicant-2.9-2.el8.x86_64.rpm \
          "

SRC_URI[wpa_supplicant.sha256sum] = "b6d2121aee6d4837393678d0beee77c69c3c9758ba81c86c8192622344e24141"
