SUMMARY = "generated recipe based on anaconda-user-help srpm"
DESCRIPTION = "Description"
LICENSE = "CC-BY-SA-1.0"
RPM_LICENSE = "CC-BY-SA"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/anaconda-user-help-8.2.3-1.el8.noarch.rpm \
          "

SRC_URI[anaconda-user-help.sha256sum] = "759494579a7e5dfe707afb484ae31da778c1560958956fb0776af9f2a2b8f961"
