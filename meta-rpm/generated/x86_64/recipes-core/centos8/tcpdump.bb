SUMMARY = "generated recipe based on tcpdump srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "BSD with advertising"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libpcap openssl pkgconfig-native"
RPM_SONAME_REQ_tcpdump = "libc.so.6 libcrypto.so.1.1 libpcap.so.1"
RDEPENDS_tcpdump = "bash glibc libpcap openssl-libs shadow-utils"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/tcpdump-4.9.2-6.el8.x86_64.rpm \
          "

SRC_URI[tcpdump.sha256sum] = "6dde2f59a1e09a2d16a590d47e065fb31ce99411cc272c758eda7bffa4b31252"
