SUMMARY = "generated recipe based on perl-PathTools srpm"
DESCRIPTION = "Description"
LICENSE = "(GPL-2.0 | Artistic-1.0) & BSD"
RPM_LICENSE = "(GPL+ or Artistic) and BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-PathTools = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-PathTools = "glibc perl-Carp perl-Errno perl-Exporter perl-Scalar-List-Utils perl-constant perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/perl-PathTools-3.74-1.el8.x86_64.rpm \
          "

SRC_URI[perl-PathTools.sha256sum] = "512245f7741790b36b03562469b9262f4dedfb8862dfa2d42e64598bb205d4c9"
