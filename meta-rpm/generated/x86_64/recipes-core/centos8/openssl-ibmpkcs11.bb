SUMMARY = "generated recipe based on openssl-ibmpkcs11 srpm"
DESCRIPTION = "Description"
LICENSE = "OpenSSL"
RPM_LICENSE = "OpenSSL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "openssl pkgconfig-native"
RPM_SONAME_REQ_openssl-ibmpkcs11 = "libc.so.6 libcrypto.so.1.1 libdl.so.2 libpthread.so.0"
RDEPENDS_openssl-ibmpkcs11 = "glibc opencryptoki-libs openssl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/openssl-ibmpkcs11-1.0.2-1.el8.x86_64.rpm \
          "

SRC_URI[openssl-ibmpkcs11.sha256sum] = "6ff8982d27abfe6ab98c817193b676f8eb34d1f941378af578c177dfd0c2ab90"
