SUMMARY = "generated recipe based on python-docutils srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED & BSD & Python-2.0 & GPL-3.0"
RPM_LICENSE = "Public Domain and BSD and Python and GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-docutils = "platform-python python36"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python3-docutils-0.14-12.module_el8.1.0+245+c39af44f.noarch.rpm \
          "

SRC_URI[python3-docutils.sha256sum] = "29a40da4b81206a931ce543357950e1032ab561b7b0f43181b06194b7fcd73e5"
