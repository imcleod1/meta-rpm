SUMMARY = "generated recipe based on perl-Term-Size-Perl srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Term-Size-Perl = "perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Term-Size-Perl-0.031-1.el8.x86_64.rpm \
          "

SRC_URI[perl-Term-Size-Perl.sha256sum] = "11ac94610db8ff012c4d255f688e74327b6729425546e8a5c90ae43cc339b03b"
