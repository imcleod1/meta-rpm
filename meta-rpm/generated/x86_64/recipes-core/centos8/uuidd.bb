SUMMARY = "generated recipe based on util-linux srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libuuid pkgconfig-native systemd-libs"
RPM_SONAME_REQ_uuidd = "libc.so.6 librt.so.1 libsystemd.so.0 libuuid.so.1"
RDEPENDS_uuidd = "bash glibc libuuid shadow-utils systemd systemd-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/uuidd-2.32.1-22.el8.x86_64.rpm \
          "

SRC_URI[uuidd.sha256sum] = "24f87c11e5b2b1fcf514f7e75d2bef95889e2e0d16af8b6d0b1c90c98267274a"
