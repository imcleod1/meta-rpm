SUMMARY = "generated recipe based on perl-Test-Fatal srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Test-Fatal = "perl-Carp perl-Exporter perl-Test-Simple perl-Try-Tiny perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Test-Fatal-0.014-9.el8.noarch.rpm \
          "

SRC_URI[perl-Test-Fatal.sha256sum] = "1e9a51822fa96f36419a6c4d307dee70449481a79e3e18ed4feffbf4d4b15944"
