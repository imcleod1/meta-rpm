SUMMARY = "generated recipe based on perl-Exception-Class srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Exception-Class = "perl-Class-Data-Inheritable perl-Devel-StackTrace perl-Scalar-List-Utils perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Exception-Class-1.44-2.el8.noarch.rpm \
          "

SRC_URI[perl-Exception-Class.sha256sum] = "09cc6c7ab8b16efe1cc26d0db0887e47b57f4c1ce5292dd459141f13c58f6855"
