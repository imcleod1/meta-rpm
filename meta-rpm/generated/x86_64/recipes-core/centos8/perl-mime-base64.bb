SUMMARY = "generated recipe based on perl-MIME-Base64 srpm"
DESCRIPTION = "Description"
LICENSE = "(GPL-2.0 | Artistic-1.0) & MIT"
RPM_LICENSE = "(GPL+ or Artistic) and MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-MIME-Base64 = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-MIME-Base64 = "glibc perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/perl-MIME-Base64-3.15-396.el8.x86_64.rpm \
          "

SRC_URI[perl-MIME-Base64.sha256sum] = "5642297bf32bb174173917dd10fd2a3a2ef7277c599f76c0669c5c448f10bdaf"
