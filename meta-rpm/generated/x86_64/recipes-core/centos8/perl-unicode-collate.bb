SUMMARY = "generated recipe based on perl-Unicode-Collate srpm"
DESCRIPTION = "Description"
LICENSE = "(GPL-2.0 | Artistic-1.0) & CLOSED"
RPM_LICENSE = "(GPL+ or Artistic) and Unicode"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Unicode-Collate = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Unicode-Collate = "glibc perl-Carp perl-PathTools perl-Unicode-Normalize perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Unicode-Collate-1.25-2.el8.x86_64.rpm \
          "

SRC_URI[perl-Unicode-Collate.sha256sum] = "6f39c0df2bc133453f365700f7fb69cf527e267861c7b411f800da937f149b14"
