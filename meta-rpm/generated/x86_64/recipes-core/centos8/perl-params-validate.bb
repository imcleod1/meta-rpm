SUMMARY = "generated recipe based on perl-Params-Validate srpm"
DESCRIPTION = "Description"
LICENSE = "Artistic-2.0 & (GPL-2.0 | Artistic-1.0)"
RPM_LICENSE = "Artistic 2.0 and (GPL+ or Artistic)"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Params-Validate = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Params-Validate = "glibc perl-Carp perl-Exporter perl-Module-Implementation perl-Scalar-List-Utils perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Params-Validate-1.29-5.el8.x86_64.rpm \
          "

SRC_URI[perl-Params-Validate.sha256sum] = "c35b54a8208dd05fa567cde52fa5e262ca6588c0f999004f6280e928f007b208"
