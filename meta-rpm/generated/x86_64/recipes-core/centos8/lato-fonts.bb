SUMMARY = "generated recipe based on lato-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "OFL-1.1"
RPM_LICENSE = "OFL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_lato-fonts = "fontpackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/lato-fonts-2.015-5.el8.noarch.rpm \
          "

SRC_URI[lato-fonts.sha256sum] = "fbffd5efd635754cabbad344a0960fbd6e0e0c64767f5f235a69e86683af8395"
