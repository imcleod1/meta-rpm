SUMMARY = "generated recipe based on maven-plugin-bundle srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_maven-plugin-bundle = "aqute-bndlib felix-utils java-1.8.0-openjdk-headless javapackages-filesystem maven-archiver maven-dependency-tree maven-lib osgi-core plexus-build-api plexus-utils"
RDEPENDS_maven-plugin-bundle-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-plugin-bundle-3.5.0-2.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-plugin-bundle-javadoc-3.5.0-2.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[maven-plugin-bundle.sha256sum] = "69c8a3a6f35bfb47bf64c9a25ac7baa393a82d0889f3a676f8fddcd67a0fbca8"
SRC_URI[maven-plugin-bundle-javadoc.sha256sum] = "eb9cab20dc04200c4dd4792d021a41144b970c2b903bc8f2801e04ad668628c9"
