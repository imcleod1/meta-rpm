SUMMARY = "generated recipe based on hunspell-af srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-af = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-af-0.20080825-16.el8.noarch.rpm \
          "

SRC_URI[hunspell-af.sha256sum] = "94c07749804e7e1e8b25be0e4352c9b43260fd77f39969883da9bd8476081815"
