SUMMARY = "generated recipe based on sed srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "acl libselinux pkgconfig-native"
RPM_SONAME_REQ_sed = "libacl.so.1 libc.so.6 libselinux.so.1"
RDEPENDS_sed = "bash glibc info libacl libselinux"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/sed-4.5-1.el8.x86_64.rpm \
          "

SRC_URI[sed.sha256sum] = "1d183613170389194490f3bcafac482267b8d4e4c93118cb46f41bba5d9bfa42"
