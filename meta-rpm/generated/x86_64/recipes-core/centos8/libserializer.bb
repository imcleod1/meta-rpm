SUMMARY = "generated recipe based on libserializer srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_libserializer = "java-1.8.0-openjdk-headless javapackages-tools libbase"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libserializer-1.1.2-18.el8.noarch.rpm \
          "

SRC_URI[libserializer.sha256sum] = "441097b46204c5f3b60b9f6c5a38115fdf3e990bad6f656e441789ab69d8359b"
