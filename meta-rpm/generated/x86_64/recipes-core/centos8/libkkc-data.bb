SUMMARY = "generated recipe based on libkkc-data srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libkkc-data-0.2.7-12.el8.x86_64.rpm \
          "

SRC_URI[libkkc-data.sha256sum] = "6e82100bb96c83b8b0d45481281364634b72ba79b1f8e45c3ffac7c9a2685f2d"
