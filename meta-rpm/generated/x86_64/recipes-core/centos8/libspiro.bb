SUMMARY = "generated recipe based on libspiro srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libspiro = "libspiro.so.0"
RPM_SONAME_REQ_libspiro = "libc.so.6 libm.so.6"
RDEPENDS_libspiro = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libspiro-20150131-8.el8.x86_64.rpm \
          "

SRC_URI[libspiro.sha256sum] = "84b5c19a5b037c06671e09a82336c664ecc3661efa8ec1ca1c0f083373f5b377"
