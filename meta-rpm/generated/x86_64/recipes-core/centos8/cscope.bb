SUMMARY = "generated recipe based on cscope srpm"
DESCRIPTION = "Description"
LICENSE = "BSD & GPL-2.0"
RPM_LICENSE = "BSD and GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "ncurses pkgconfig-native"
RPM_SONAME_REQ_cscope = "libc.so.6 libncurses.so.6 libtinfo.so.6"
RDEPENDS_cscope = "bash emacs-filesystem glibc ncurses-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/cscope-15.9-6.el8.x86_64.rpm \
          "

SRC_URI[cscope.sha256sum] = "aab9d3ca883ecaad7b4fb97fff8147a28e2aa15e8556d80ebf729b3ae4db246f"
