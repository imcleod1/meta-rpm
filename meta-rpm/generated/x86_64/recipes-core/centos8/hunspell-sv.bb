SUMMARY = "generated recipe based on hunspell-sv srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-3.0"
RPM_LICENSE = "LGPLv3"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-sv = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-sv-2.28-8.el8.noarch.rpm \
          "

SRC_URI[hunspell-sv.sha256sum] = "3432f086038f8e9b99a3be10a2de268b1c1d5da5bb7dcc8b95331c834983607a"
