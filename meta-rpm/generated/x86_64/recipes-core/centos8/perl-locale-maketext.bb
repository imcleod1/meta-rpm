SUMMARY = "generated recipe based on perl-Locale-Maketext srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Locale-Maketext = "perl-Carp perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Locale-Maketext-1.28-396.el8.noarch.rpm \
          "

SRC_URI[perl-Locale-Maketext.sha256sum] = "876f742fb6935712ca9e009225076e0b1135b17199060c447e989320e8850684"
