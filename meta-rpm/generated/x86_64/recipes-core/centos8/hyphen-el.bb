SUMMARY = "generated recipe based on hyphen-el srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0"
RPM_LICENSE = "LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hyphen-el = "hyphen"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hyphen-el-0.20051018-17.el8.noarch.rpm \
          "

SRC_URI[hyphen-el.sha256sum] = "357670e44dc5dee6091713ff84c334c3004cde2074d79a0a071b1edf095b004b"
