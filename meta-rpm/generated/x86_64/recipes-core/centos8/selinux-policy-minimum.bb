SUMMARY = "generated recipe based on selinux-policy srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_selinux-policy-minimum = "bash coreutils policycoreutils-python-utils selinux-policy"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/selinux-policy-minimum-3.14.3-41.el8_2.8.noarch.rpm \
          "

SRC_URI[selinux-policy-minimum.sha256sum] = "73b4927e0dc4cb5aca0cfc9b1510294cdec5cf875289a2852715f07eb6c38666"
