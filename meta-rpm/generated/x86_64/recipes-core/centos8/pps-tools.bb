SUMMARY = "generated recipe based on pps-tools srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPROVIDES_pps-tools-devel = "pps-tools-dev (= 1.0.2)"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/pps-tools-devel-1.0.2-1.el8.x86_64.rpm \
          "

SRC_URI[pps-tools-devel.sha256sum] = "3c37ef5df536c0b51a122486ddab108060153718be986b4579eb95b5564e8093"
