SUMMARY = "generated recipe based on emacs srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0 & CC0-1.0"
RPM_LICENSE = "GPLv3+ and CC0-1.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/emacs-filesystem-26.1-5.el8.noarch.rpm \
          "

SRC_URI[emacs-filesystem.sha256sum] = "b7b3f2a6ea64d0c7c3afb64c3740d68b9fd6e28a976f5867e0be14682fc494a9"
