SUMMARY = "generated recipe based on kurdit-unikurd-web-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_kurdit-unikurd-web-fonts = "fontpackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/kurdit-unikurd-web-fonts-20020502-19.el8.noarch.rpm \
          "

SRC_URI[kurdit-unikurd-web-fonts.sha256sum] = "9dd54052b41991520473e1748f1804dd499d11a53f8e098fec15133754df948f"
