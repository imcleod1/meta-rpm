SUMMARY = "generated recipe based on acpica-tools srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_acpica-tools = "libc.so.6 libpthread.so.0 librt.so.1"
RDEPENDS_acpica-tools = "bash glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/acpica-tools-20180629-3.el8.x86_64.rpm \
          "

SRC_URI[acpica-tools.sha256sum] = "2656fa459b70981e9615e113f000b95497c58b8359fb007f7feb8f08c45155e0"
