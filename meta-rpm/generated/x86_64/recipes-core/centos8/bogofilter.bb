SUMMARY = "generated recipe based on bogofilter srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "db gsl pkgconfig-native"
RPM_SONAME_REQ_bogofilter = "libc.so.6 libdb-5.3.so libgsl.so.23 libgslcblas.so.0 libm.so.6"
RDEPENDS_bogofilter = "bash glibc gsl libdb"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/bogofilter-1.2.4-13.el8.x86_64.rpm \
          "

SRC_URI[bogofilter.sha256sum] = "a149fb0c780864ed141040b7d7f58784cb879140aa5bb92befde8c05d872dfe5"
