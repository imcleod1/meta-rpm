SUMMARY = "generated recipe based on libtar srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native zlib"
RPM_SONAME_PROV_libtar = "libtar.so.1"
RPM_SONAME_REQ_libtar = "libc.so.6 libz.so.1"
RDEPENDS_libtar = "glibc zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libtar-1.2.20-15.el8.x86_64.rpm \
          "

SRC_URI[libtar.sha256sum] = "1a2d40d63a5c65ba7d7392128371459cc01ca7a6f7200d32e05d940ca1cc95f1"
