SUMMARY = "generated recipe based on hunspell-mai srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | LGPL-2.0 | MPL-1.1"
RPM_LICENSE = "GPLv2+ or LGPLv2+ or MPLv1.1"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-mai = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-mai-1.0.1-15.el8.noarch.rpm \
          "

SRC_URI[hunspell-mai.sha256sum] = "b1a91834b9f8a73b7ad984d0ab98b8ff1150c607e1d59bb866d0832cf317d57d"
