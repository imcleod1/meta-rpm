SUMMARY = "generated recipe based on gcc-toolset-9-strace srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.1 & GPL-2.0"
RPM_LICENSE = "LGPL-2.1+ and GPL-2.0+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "bzip2 pkgconfig-native xz zlib"
RPM_SONAME_REQ_gcc-toolset-9-strace = "ld-linux-x86-64.so.2 libbz2.so.1 libc.so.6 liblzma.so.5 librt.so.1 libz.so.1"
RDEPENDS_gcc-toolset-9-strace = "bash bzip2-libs gcc-toolset-9-runtime glibc xz-libs zlib"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/gcc-toolset-9-strace-5.1-6.el8.x86_64.rpm \
          "

SRC_URI[gcc-toolset-9-strace.sha256sum] = "9c0c4d8abe859d0825bdcb54670b768681e58f979d0db83e21ebc4cdfc1abded"
