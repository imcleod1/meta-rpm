SUMMARY = "generated recipe based on glibc srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & LGPL-2.0 & GPL-2.0 & GPL-2.0 & BSD & CLOSED & ISC & CLOSED & GFDL-1.1"
RPM_LICENSE = "LGPLv2+ and LGPLv2+ with exceptions and GPLv2+ and GPLv2+ with exceptions and BSD and Inner-Net and ISC and Public Domain and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPROVIDES_glibc-langpack-ca = "locale-base-ca-ad (= 2.28) locale-base-ca-ad.utf8 (= 2.28) locale-base-ca-es (= 2.28) locale-base-ca-es.utf8 (= 2.28) locale-base-ca-es@euro (= 2.28) locale-base-ca-es@valencia (= 2.28) locale-base-ca-fr (= 2.28) locale-base-ca-fr.utf8 (= 2.28) locale-base-ca-it (= 2.28) locale-base-ca-it.utf8 (= 2.28) virtual-locale-ca (= 2.28) virtual-locale-ca (= 2.28) virtual-locale-ca (= 2.28) virtual-locale-ca (= 2.28) virtual-locale-ca (= 2.28) virtual-locale-ca (= 2.28) virtual-locale-ca (= 2.28) virtual-locale-ca (= 2.28) virtual-locale-ca (= 2.28) virtual-locale-ca (= 2.28) virtual-locale-ca-ad (= 2.28) virtual-locale-ca-ad.utf8 (= 2.28) virtual-locale-ca-es (= 2.28) virtual-locale-ca-es.utf8 (= 2.28) virtual-locale-ca-es@euro (= 2.28) virtual-locale-ca-es@valencia (= 2.28) virtual-locale-ca-fr (= 2.28) virtual-locale-ca-fr.utf8 (= 2.28) virtual-locale-ca-it (= 2.28) virtual-locale-ca-it.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-ca = "glibc glibc-common"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-ca-2.28-101.el8.x86_64.rpm \
          "

SRC_URI[glibc-langpack-ca.sha256sum] = "5c04a179359845fd6e01bf17f2254fa332ef6896e198a22f2990abc9daa6e7ab"
