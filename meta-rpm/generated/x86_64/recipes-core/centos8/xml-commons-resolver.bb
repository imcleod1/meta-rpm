SUMMARY = "generated recipe based on xml-commons-resolver srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_xml-commons-resolver = "bash java-1.8.0-openjdk-headless javapackages-filesystem javapackages-tools"
RDEPENDS_xml-commons-resolver-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/xml-commons-resolver-1.2-26.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/xml-commons-resolver-javadoc-1.2-26.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[xml-commons-resolver.sha256sum] = "50b4f4c2b8ccc28c1221f1715e51f03afb0b704c535c41db8df942696cb56a8b"
SRC_URI[xml-commons-resolver-javadoc.sha256sum] = "6a71f037b7467a33ccf49a8bc7066e9b4a93b534b2d8ed2c16ba0fff8484f1f4"
