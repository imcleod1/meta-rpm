SUMMARY = "generated recipe based on glibc srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & LGPL-2.0 & GPL-2.0 & GPL-2.0 & BSD & CLOSED & ISC & CLOSED & GFDL-1.1"
RPM_LICENSE = "LGPLv2+ and LGPLv2+ with exceptions and GPLv2+ and GPLv2+ with exceptions and BSD and Inner-Net and ISC and Public Domain and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPROVIDES_glibc-langpack-kab = "locale-base-kab-dz (= 2.28) virtual-locale-kab (= 2.28) virtual-locale-kab-dz (= 2.28)"
RDEPENDS_glibc-langpack-kab = "glibc glibc-common"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-kab-2.28-101.el8.x86_64.rpm \
          "

SRC_URI[glibc-langpack-kab.sha256sum] = "ec9045f60c97dec4f38bc7261b9f0e7bb5463cbbe54e67406d18abbaec02e7a4"
