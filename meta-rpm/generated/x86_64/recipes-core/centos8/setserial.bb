SUMMARY = "generated recipe based on setserial srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPL+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_setserial = "libc.so.6"
RDEPENDS_setserial = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/setserial-2.17-45.el8.x86_64.rpm \
          "

SRC_URI[setserial.sha256sum] = "8ac083cbc432df1db9e6f3a34b426946625a3bb03601e492bceb490d5366736d"
