SUMMARY = "generated recipe based on xorg-x11-drv-dummy srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_REQ_xorg-x11-drv-dummy = "libc.so.6"
RDEPENDS_xorg-x11-drv-dummy = "glibc xorg-x11-server-Xorg"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/xorg-x11-drv-dummy-0.3.7-6.el8.1.x86_64.rpm \
          "

SRC_URI[xorg-x11-drv-dummy.sha256sum] = "26ea3fdd2567806d8d6487f66afff67148fc63553dc773575961bc89241ff5df"
