SUMMARY = "generated recipe based on sbc srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 & LGPL-2.0"
RPM_LICENSE = "GPLv2 and LGPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_sbc = "libsbc.so.1"
RPM_SONAME_REQ_sbc = "libc.so.6"
RDEPENDS_sbc = "glibc"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/sbc-1.3-9.el8.x86_64.rpm \
          "

SRC_URI[sbc.sha256sum] = "8e3fc1308cca65e918af46bcf903a995295aab0b6a8b4976bc152d06efdeba15"
