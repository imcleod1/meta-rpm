SUMMARY = "generated recipe based on python-linux-procfs srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-linux-procfs = "platform-python python3-six"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-linux-procfs-0.6-7.el8.noarch.rpm \
          "

SRC_URI[python3-linux-procfs.sha256sum] = "5226e65270b5a04b649ce2b7a74ed57ee390a8120a6057fe77fa535740992e27"
