SUMMARY = "generated recipe based on mesa-demos srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libglvnd libx11 pkgconfig-native"
RPM_SONAME_REQ_glx-utils = "libGL.so.1 libX11.so.6 libc.so.6 libm.so.6"
RDEPENDS_glx-utils = "glibc libX11 libglvnd-glx"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/glx-utils-8.4.0-4.20181118git1830dcb.el8.x86_64.rpm \
          "

SRC_URI[glx-utils.sha256sum] = "228fa8dfac584c0fddcb6f30a88310198aa9a082a01468fd4d61b626073df574"
