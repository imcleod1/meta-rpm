SUMMARY = "generated recipe based on hunspell-sk srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 | GPL-2.0 | MPL-1.1"
RPM_LICENSE = "LGPLv2 or GPLv2 or MPLv1.1"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-sk = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-sk-0.20110228-12.el8.noarch.rpm \
          "

SRC_URI[hunspell-sk.sha256sum] = "4e81e07b527e39f5be57c7f5435ea67f598400dba7e44ea1dd37ddb58c59afc3"
