SUMMARY = "generated recipe based on perl-XML-XPath srpm"
DESCRIPTION = "Description"
LICENSE = "Artistic-2.0 & (GPL-2.0 | Artistic-1.0)"
RPM_LICENSE = "Artistic 2.0 and (GPL+ or Artistic)"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-XML-XPath = "perl-Carp perl-Data-Dumper perl-Exporter perl-IO perl-Scalar-List-Utils perl-XML-Parser perl-interpreter perl-libs perl-open"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-XML-XPath-1.42-3.el8.noarch.rpm \
          "

SRC_URI[perl-XML-XPath.sha256sum] = "fe4f67d03097b22469cbb72e653e4ae7c1c7da300bfbdf8c454d2cf59ee3f34c"
