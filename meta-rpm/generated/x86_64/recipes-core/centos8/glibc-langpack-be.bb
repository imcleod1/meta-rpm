SUMMARY = "generated recipe based on glibc srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & LGPL-2.0 & GPL-2.0 & GPL-2.0 & BSD & CLOSED & ISC & CLOSED & GFDL-1.1"
RPM_LICENSE = "LGPLv2+ and LGPLv2+ with exceptions and GPLv2+ and GPLv2+ with exceptions and BSD and Inner-Net and ISC and Public Domain and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPROVIDES_glibc-langpack-be = "locale-base-be-by (= 2.28) locale-base-be-by.utf8 (= 2.28) locale-base-be-by@latin (= 2.28) virtual-locale-be (= 2.28) virtual-locale-be (= 2.28) virtual-locale-be (= 2.28) virtual-locale-be-by (= 2.28) virtual-locale-be-by.utf8 (= 2.28) virtual-locale-be-by@latin (= 2.28)"
RDEPENDS_glibc-langpack-be = "glibc glibc-common"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-be-2.28-101.el8.x86_64.rpm \
          "

SRC_URI[glibc-langpack-be.sha256sum] = "3838ff0bb183b2c91ee45cf8563566baa4d4121b811df38bbc739e8847c440c0"
