SUMMARY = "generated recipe based on perl-Class-Singleton srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Class-Singleton = "perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/perl-Class-Singleton-1.5-9.el8.noarch.rpm \
          "

SRC_URI[perl-Class-Singleton.sha256sum] = "0e997e8aa482676677e10ba08f7e96729f592210688c5d2f6387e83ba1130bdb"
