SUMMARY = "generated recipe based on glibc srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & LGPL-2.0 & GPL-2.0 & GPL-2.0 & BSD & CLOSED & ISC & CLOSED & GFDL-1.1"
RPM_LICENSE = "LGPLv2+ and LGPLv2+ with exceptions and GPLv2+ and GPLv2+ with exceptions and BSD and Inner-Net and ISC and Public Domain and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPROVIDES_glibc-langpack-br = "locale-base-br-fr (= 2.28) locale-base-br-fr.utf8 (= 2.28) locale-base-br-fr@euro (= 2.28) virtual-locale-br (= 2.28) virtual-locale-br (= 2.28) virtual-locale-br (= 2.28) virtual-locale-br-fr (= 2.28) virtual-locale-br-fr.utf8 (= 2.28) virtual-locale-br-fr@euro (= 2.28)"
RDEPENDS_glibc-langpack-br = "glibc glibc-common"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-br-2.28-101.el8.x86_64.rpm \
          "

SRC_URI[glibc-langpack-br.sha256sum] = "d84ec21a74399147a00f7f8577526beabd45c9b274d792d5bdd5ce3db74cb358"
