SUMMARY = "generated recipe based on libxshmfence srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPM_SONAME_PROV_libxshmfence = "libxshmfence.so.1"
RPM_SONAME_REQ_libxshmfence = "libc.so.6"
RDEPENDS_libxshmfence = "glibc"
RPM_SONAME_REQ_libxshmfence-devel = "libxshmfence.so.1"
RPROVIDES_libxshmfence-devel = "libxshmfence-dev (= 1.3)"
RDEPENDS_libxshmfence-devel = "libxshmfence pkgconf-pkg-config"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libxshmfence-1.3-2.el8.x86_64.rpm \
           https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/libxshmfence-devel-1.3-2.el8.x86_64.rpm \
          "

SRC_URI[libxshmfence.sha256sum] = "bfb818e14cfa05d800f1131366ee8fd0c30ab0c735470c870e62dabb7d3f1073"
SRC_URI[libxshmfence-devel.sha256sum] = "791a91b99259a8057b0159ef6ffc38a8496e5a74e5c65aabd53c98bd32b03bde"
