SUMMARY = "generated recipe based on pyparsing srpm"
DESCRIPTION = "Description"
LICENSE = "MIT"
RPM_LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python3-pyparsing = "platform-python"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/python3-pyparsing-2.1.10-7.el8.noarch.rpm \
          "

SRC_URI[python3-pyparsing.sha256sum] = "7541ae8ece47e31d763583e5bf2ec020e5986944d00053414bc15b5ac6233b38"
