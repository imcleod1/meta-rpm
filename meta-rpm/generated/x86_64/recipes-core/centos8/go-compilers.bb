SUMMARY = "generated recipe based on go-compilers srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/go-compilers-golang-compiler-1-20.el8.x86_64.rpm \
          "

SRC_URI[go-compilers-golang-compiler.sha256sum] = "dde711f82d66040e18a0691b40baa8f22ef8228c3e5be1a7cf03efd8e10dbcd0"
