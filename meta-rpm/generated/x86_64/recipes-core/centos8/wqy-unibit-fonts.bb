SUMMARY = "generated recipe based on wqy-unibit-fonts srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2 with exceptions"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_wqy-unibit-fonts = "fontpackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/wqy-unibit-fonts-1.1.0-20.el8.noarch.rpm \
          "

SRC_URI[wqy-unibit-fonts.sha256sum] = "9b2afc573f1660ff31cadf0e5ce7bbf3965efe8c869973d2c5458e31c6f546a1"
