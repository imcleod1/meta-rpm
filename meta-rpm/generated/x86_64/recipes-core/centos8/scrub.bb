SUMMARY = "generated recipe based on scrub srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPLv2+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcrypt libgpg-error pkgconfig-native"
RPM_SONAME_REQ_scrub = "libc.so.6 libdl.so.2 libgcrypt.so.20 libgpg-error.so.0 libpthread.so.0"
RDEPENDS_scrub = "glibc libgcrypt libgpg-error"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/scrub-2.5.2-14.el8.x86_64.rpm \
          "

SRC_URI[scrub.sha256sum] = "4973c48ebe26e5d97095abe45e4f628521589e310bfa3e1a3387e166d7ab8adc"
