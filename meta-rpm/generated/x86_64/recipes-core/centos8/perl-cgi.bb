SUMMARY = "generated recipe based on perl-CGI srpm"
DESCRIPTION = "Description"
LICENSE = "(GPL-2.0 | Artistic-1.0) & Artistic-2.0"
RPM_LICENSE = "(GPL+ or Artistic) and Artistic 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-CGI = "perl-Carp perl-Exporter perl-File-Temp perl-HTML-Parser perl-PathTools perl-Text-ParseWords perl-interpreter perl-libs perl-parent"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-CGI-4.38-2.el8.noarch.rpm \
          "

SRC_URI[perl-CGI.sha256sum] = "f3889a07e7e850185a393fbe0618dff51cdf6da0300b1622edd7043ff320dc02"
