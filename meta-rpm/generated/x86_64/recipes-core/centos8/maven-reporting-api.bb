SUMMARY = "generated recipe based on maven-reporting-api srpm"
DESCRIPTION = "Description"
LICENSE = "CLOSED"
RPM_LICENSE = "ASL 2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_maven-reporting-api = "java-1.8.0-openjdk-headless javapackages-filesystem maven-doxia-sink-api"
RDEPENDS_maven-reporting-api-javadoc = "javapackages-filesystem"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-reporting-api-3.0-14.module_el8.0.0+30+832da3a1.noarch.rpm \
           https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/maven-reporting-api-javadoc-3.0-14.module_el8.0.0+30+832da3a1.noarch.rpm \
          "

SRC_URI[maven-reporting-api.sha256sum] = "ead38ecff25a4488c59b0872e5459c2c185de3007785a38a93b2221017c554a6"
SRC_URI[maven-reporting-api-javadoc.sha256sum] = "9215ef0de2f375fe1522a945963c70626f5c2e90ec214596fd48b0c658f5466f"
