SUMMARY = "generated recipe based on perl-Sys-MemInfo srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "perl pkgconfig-native"
RPM_SONAME_REQ_perl-Sys-MemInfo = "libc.so.6 libperl.so.5.26 libpthread.so.0"
RDEPENDS_perl-Sys-MemInfo = "glibc perl-Exporter perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/perl-Sys-MemInfo-0.99-6.el8.x86_64.rpm \
          "

SRC_URI[perl-Sys-MemInfo.sha256sum] = "cdda85cafa1ccf6bfcf633345d8cc871fb3afb43d65c6f303e311e29d1cd6984"
