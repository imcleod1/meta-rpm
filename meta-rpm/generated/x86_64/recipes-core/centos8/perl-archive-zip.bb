SUMMARY = "generated recipe based on perl-Archive-Zip srpm"
DESCRIPTION = "Description"
LICENSE = "(GPL-2.0 | Artistic-1.0) & BSD"
RPM_LICENSE = "(GPL+ or Artistic) and BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Archive-Zip = "perl-Carp perl-Compress-Raw-Zlib perl-Data-Dumper perl-Encode perl-Exporter perl-File-Path perl-File-Temp perl-IO perl-PathTools perl-Time-Local perl-constant perl-interpreter perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Archive-Zip-1.60-3.el8.noarch.rpm \
          "

SRC_URI[perl-Archive-Zip.sha256sum] = "e2a7aa697857f021a644d566b81b75f04892e77ddcb821ff34bae531b45b2959"
