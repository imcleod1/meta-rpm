SUMMARY = "generated recipe based on hunspell-it srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-it = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-it-2.4-0.17.20070901.el8.noarch.rpm \
          "

SRC_URI[hunspell-it.sha256sum] = "151c0c22cf49b5d2b3453935022519be0331975e5f586ffb2ba17bf16a11e1c1"
