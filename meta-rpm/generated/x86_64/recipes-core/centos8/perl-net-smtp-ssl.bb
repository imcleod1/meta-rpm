SUMMARY = "generated recipe based on perl-Net-SMTP-SSL srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0 | Artistic-1.0"
RPM_LICENSE = "GPL+ or Artistic"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_perl-Net-SMTP-SSL = "perl-IO-Socket-SSL perl-libnet perl-libs"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/perl-Net-SMTP-SSL-1.04-5.el8.noarch.rpm \
          "

SRC_URI[perl-Net-SMTP-SSL.sha256sum] = "2b88d6996a485bfadaf9324625c56d693b600ce349c331bc0c6b7427de393ad9"
