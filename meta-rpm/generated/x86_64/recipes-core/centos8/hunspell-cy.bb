SUMMARY = "generated recipe based on hunspell-cy srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-2.0"
RPM_LICENSE = "GPL+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_hunspell-cy = "hunspell"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/hunspell-cy-0.20040425-18.el8.noarch.rpm \
          "

SRC_URI[hunspell-cy.sha256sum] = "bfd67100b8ef7c77debea1ec79d8307fb4734705ea96876942f696ec5251deae"
