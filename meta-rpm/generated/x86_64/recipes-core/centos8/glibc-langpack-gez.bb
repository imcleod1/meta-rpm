SUMMARY = "generated recipe based on glibc srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & LGPL-2.0 & GPL-2.0 & GPL-2.0 & BSD & CLOSED & ISC & CLOSED & GFDL-1.1"
RPM_LICENSE = "LGPLv2+ and LGPLv2+ with exceptions and GPLv2+ and GPLv2+ with exceptions and BSD and Inner-Net and ISC and Public Domain and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPROVIDES_glibc-langpack-gez = "locale-base-gez-er (= 2.28) locale-base-gez-er@abegede (= 2.28) locale-base-gez-et (= 2.28) locale-base-gez-et@abegede (= 2.28) virtual-locale-gez (= 2.28) virtual-locale-gez (= 2.28) virtual-locale-gez (= 2.28) virtual-locale-gez (= 2.28) virtual-locale-gez-er (= 2.28) virtual-locale-gez-er@abegede (= 2.28) virtual-locale-gez-et (= 2.28) virtual-locale-gez-et@abegede (= 2.28)"
RDEPENDS_glibc-langpack-gez = "glibc glibc-common"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-gez-2.28-101.el8.x86_64.rpm \
          "

SRC_URI[glibc-langpack-gez.sha256sum] = "6c3d61ff78f73f268a470e20e765b038936008590b4ea4c9e47c62e6dba65108"
