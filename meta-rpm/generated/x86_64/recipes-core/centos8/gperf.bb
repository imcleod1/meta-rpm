SUMMARY = "generated recipe based on gperf srpm"
DESCRIPTION = "Description"
LICENSE = "GPL-3.0"
RPM_LICENSE = "GPLv3+"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "libgcc pkgconfig-native"
RPM_SONAME_REQ_gperf = "libc.so.6 libgcc_s.so.1 libm.so.6 libstdc++.so.6"
RDEPENDS_gperf = "bash glibc info libgcc libstdc++"

RPM_URI = "https://vault.centos.org/8.2.2004/PowerTools/x86_64/os/Packages/gperf-3.1-5.el8.x86_64.rpm \
          "

SRC_URI[gperf.sha256sum] = "ddf09cf0962cde7e9bb08ca09bc268b803d73998372c95b151384cbfe1799948"
