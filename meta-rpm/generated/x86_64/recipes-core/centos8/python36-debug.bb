SUMMARY = "generated recipe based on python36 srpm"
DESCRIPTION = "Description"
LICENSE = "Python-2.0"
RPM_LICENSE = "Python"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_python36-debug = "bash platform-python-debug python36 python36-devel"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/python36-debug-3.6.8-2.module_el8.1.0+245+c39af44f.x86_64.rpm \
          "

SRC_URI[python36-debug.sha256sum] = "4fc56f87715092491e552568c67dee991395564515201ea2c7033701e8897715"
