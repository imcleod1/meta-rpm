SUMMARY = "generated recipe based on openssl-pkcs11 srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & BSD"
RPM_LICENSE = "LGPLv2+ and BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "openssl pkgconfig-native"
RPM_SONAME_PROV_openssl-pkcs11 = "libp11.so.3"
RPM_SONAME_REQ_openssl-pkcs11 = "libc.so.6 libcrypto.so.1.1 libdl.so.2"
RDEPENDS_openssl-pkcs11 = "glibc openssl openssl-libs p11-kit-trust"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/openssl-pkcs11-0.4.10-2.el8.x86_64.rpm \
          "

SRC_URI[openssl-pkcs11.sha256sum] = "2f056611d9f9f262946d31c60c0d16158936c83f11089dd45f08d50a3781dcd4"
