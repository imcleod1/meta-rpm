SUMMARY = "generated recipe based on media-player-info srpm"
DESCRIPTION = "Description"
LICENSE = "BSD"
RPM_LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "allarch"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RDEPENDS_media-player-info = "systemd-udev"

RPM_URI = "https://vault.centos.org/8.2.2004/AppStream/x86_64/os/Packages/media-player-info-23-2.el8.noarch.rpm \
          "

SRC_URI[media-player-info.sha256sum] = "8ae106b691b0dfcbb9c0217fb4209c90960bf372b4f23f9790b6be52e3e2f29f"
