SUMMARY = "generated recipe based on glibc srpm"
DESCRIPTION = "Description"
LICENSE = "LGPL-2.0 & LGPL-2.0 & GPL-2.0 & GPL-2.0 & BSD & CLOSED & ISC & CLOSED & GFDL-1.1"
RPM_LICENSE = "LGPLv2+ and LGPLv2+ with exceptions and GPLv2+ and GPLv2+ with exceptions and BSD and Inner-Net and ISC and Public Domain and GFDL"
LIC_FILES_CHKSUM = "file://${COREBASE}/LCHKSUM;md5=2e132387ce9c8d20e4ad825d68e07eca"
PACKAGE_ARCH = "x86_64"
inherit rpmbased
BBCLASSEXTEND = "native"
DEPENDS = "pkgconfig-native"
RPROVIDES_glibc-langpack-kk = "locale-base-kk-kz (= 2.28) locale-base-kk-kz.utf8 (= 2.28) virtual-locale-kk (= 2.28) virtual-locale-kk (= 2.28) virtual-locale-kk-kz (= 2.28) virtual-locale-kk-kz.utf8 (= 2.28)"
RDEPENDS_glibc-langpack-kk = "glibc glibc-common"

RPM_URI = "https://vault.centos.org/8.2.2004/BaseOS/x86_64/os/Packages/glibc-langpack-kk-2.28-101.el8.x86_64.rpm \
          "

SRC_URI[glibc-langpack-kk.sha256sum] = "2873854c8a0e3665e3e29fad019f6627e89e592c4215c2ae6e70d33a387e8482"
