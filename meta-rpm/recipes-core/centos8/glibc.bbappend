# We enable the usrmerge distro feature so /lib64 is not staged
# However we need to recreate it in the recipe build sysroots so builds can find /lib64/ld-linux..
SYSROOT_PREPROCESS_FUNCS_append = " glibc_sysroot_preprocess"
glibc_sysroot_preprocess () {
        mkdir -p ${SYSROOT_DESTDIR}${bindir}
        dest=${SYSROOT_DESTDIR}${bindir}/postinst-${PN}
        echo "#!/bin/sh" > $dest
        echo "ln -s usr/lib64 ${STAGING_DIR_TARGET}/lib64" >> $dest
        chmod 755 $dest
}
