TOPDIR=.
BINDIR=$(TOPDIR)/bin
GENDIR=$(TOPDIR)/meta-rpm/generated

all: centos8-x86_64 centos8-aarch64

BASEURL = https://vault.centos.org/8.2.2004

# The latest version?
#BASEURL = http://mirror.centos.org/centos/8/

centos8-x86_64:
	rm -rf $(GENDIR)/x86_64/recipes-core/centos8/*
	$(BINDIR)/generate-recipes.py \
	   --mapping $(TOPDIR)/meta-rpm/recipes-core/centos8/centos8-mapping.yaml \
	   --destdir $(GENDIR)/x86_64/recipes-core/centos8/ \
	   --licenses $(TOPDIR)/meta-rpm/recipes-core/centos8/license_dict.json \
	   --repo $(BASEURL)/PowerTools/x86_64/os/ \
	   --repo $(BASEURL)/BaseOS/x86_64/os/ \
	   --repo $(BASEURL)/AppStream/x86_64/os/
	ln -s `realpath --relative-to="$(GENDIR)/x86_64/recipes-core/centos8/" $(TOPDIR)/meta-rpm/recipes-core/centos8/files` $(GENDIR)/x86_64/recipes-core/centos8/files

centos8-aarch64:
	rm -rf $(GENDIR)/aarch64/recipes-core/centos8/*
	$(BINDIR)/generate-recipes.py \
	   --mapping $(TOPDIR)/meta-rpm/recipes-core/centos8/centos8-mapping.yaml \
	   --destdir $(GENDIR)/aarch64/recipes-core/centos8/ \
	   --licenses $(TOPDIR)/meta-rpm/recipes-core/centos8/license_dict.json \
	   --repo $(BASEURL)/PowerTools/aarch64/os/ \
	   --repo $(BASEURL)/BaseOS/aarch64/os/ \
	   --repo $(BASEURL)/AppStream/aarch64/os/
	   ln -s `realpath --relative-to="$(GENDIR)/aarch64/recipes-core/centos8/" $(TOPDIR)/meta-rpm/recipes-core/centos8/files` $(GENDIR)/aarch64/recipes-core/centos8/files

